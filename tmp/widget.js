'use strict';

// <= 90 CHF                Transaction authorized
// >90 CHF and <= 100 CHF   Transaction declined (i.e. insufficient limit, bad expiry date)
// >100 CHF and <= 110 CHF  Referral
// >110 CHF                 Card blocked (lost or stolen)


// vis: 4242424242424242
// eca: 5404000000000001


class PaymentProcess {
  constructor (options) {
    this.merchantApiKey = options.merchantApiKey
    this.allowedCCTypes = options.allowedCCTypes
    this.fields = options.fields

    this.form = null
    this.flowIframe = null
    this.flowPopup = null
    this.cardIframe = null

    this.initListeners()
  }

  initListeners () {
    console.log('initListeners')

    document.addEventListener('DOMContentLoaded', (event) => {
      this.pageOnLoad(event)
    })

    window.addEventListener("message", (event) => {
      this.receiveMessage(event)
    })

    document.querySelector('button[data-action=start]').addEventListener('click', (event) => {
      this.init()
    })

    document.querySelector('button[data-action=clear]').addEventListener('click', (event) => {
      window.location.replace(window.location.origin + window.location.pathname)
    })

    document.querySelectorAll('.settings input').forEach((el) => {
      el.addEventListener('change', (event) => {
        this.init()
      })
    })
  }

  init () {
    console.log("init")

    this.flowMode = document.querySelector('.settings input[name=flowMode]:checked').value
    this.cardMode = document.querySelector('.settings input[name=cardMode]:checked').value

    this.removeForm()
    this.removeResult()

    this.insertForm()
  }

  initCCNumberChangeHandler() {
    let inputCardno = document.querySelector('.form-cont form input[name=cardno]')
    if (inputCardno) {
      inputCardno.addEventListener('keyup', (event) => {
        this.updatePaymentMethod(event.target.value)
      })
    }
  }

  updatePaymentMethod(number) {
    this.updateFormData({ payment_method: this.getCCType(number) })
  }

  pageOnLoad (event) {
    let data = {}
    try {
      data = this.getQueryParams(document.location.search)
    } catch (err) {
      console.log(err)
    }
    console.log('pageOnLoad', data)
    // if (data && data.epp_transaction_id) {
    if (data && data.epayment_status) {
      if (window.opener) {
        this.postMessage(window.opener, {topic: 'flow-close-popup', data: data})
      } else {
        this.removeForm()
        this.showResult(data)
      }
    }
  }

  async insertForm () {
    console.log('insertForm')

    let formOptions = {
      action: `https://api.stage.raisenow.net/epayment/api/step/pay/merchant/${this.merchantApiKey}`,
      method: 'post',
      insertTo: '.form-cont',
    }

    if (this.flowMode === 'redirect') {
      formOptions.target = '_self'
    } else if (this.flowMode === 'iframe') {
      formOptions.target = 'flowIframe'
    } else if (this.flowMode === 'popup') {
      formOptions.target = 'flowPopup'
    }

    this.form = window.rnw.epik.insertForm(document, formOptions)

    let fields = Object.assign({}, this.fields, this.getCardFields())
    window.rnw.epik.insertFormData(document, this.form, fields)

    this.initCCNumberChangeHandler()

    if (this.cardMode === 'token') {
      await this.insertCardIframe()
    }

    let button = document.createElement('button')
    button.textContent = 'Pay'
    button.onclick = (event) => {
      if (this.cardMode === 'form') {
        this.submitForm()
      } else if (this.cardMode === 'token') {
        this.postMessage(this.cardIframe.contentWindow, {topic: 'rnw-creditcardform.validate-form'})
      }
    }
    document.querySelector('.form-cont').appendChild(button)
  }

  getCardFields () {
    if (this.cardMode === 'form') {
      return {
        cardno: '',
        cvv: '',
        expm: '',
        expy: '',
        // cardno: '4242424242424242',
        // cvv: '123',
        // expm: '12',
        // expy: '18',
      }
    } else if (this.cardMode === 'token') {
      return {
        card_token: '',
      }
    }
  }

  async insertFlowIframe () {
    if (this.flowIframe) { return }
    let iframeOptions = {
      style: 'width: 400px; height: 400px; border: 1px dashed #ccc;',
      insertTo: '.form-cont',
      name: 'flowIframe',
      onLoad: (event) => {
        let iframe = event.path[0]
        let data = {}
        try {
          data = this.getQueryParams(iframe.contentWindow.location.search)
        } catch (err) {
          console.log(err)
        }
        console.log('iframeOnLoad', data)
        // if (data && data.epp_transaction_id) {
        if (data && data.epayment_status) {
          this.removeForm()
          this.showResult(data)
        }
      },
    }
    this.flowIframe = await window.rnw.epik.insertIframe(document, iframeOptions)
  }

  async insertCardIframe () {
    if (this.cardIframe) { return }
    let iframeOptions = {
      src: `https://api.stage.raisenow.net/epayment/api/step/select-card/merchant/${this.merchantApiKey}?submit_button_visibility=false`,
      style: 'width: 400px; height: 266px; border: 1px dashed #ccc;',
      insertTo: '.form-cont',
      name: 'cardIframe'
    }
    this.cardIframe = await window.rnw.epik.insertIframe(document, iframeOptions)
  }

  openFlowPopup () {
    if (this.flowPopup === null || this.flowPopup.closed) {
      this.flowPopup = window.open('', 'flowPopup', 'width=400,height=400,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes')
    } else {
      this.flowPopup.focus()
    }
  }

  removeForm () {
    console.log('removeForm')
    document.querySelector('.form-cont').textContent = ''
    if (this.flowPopup) {
      this.flowPopup.close()
    }
    this.flowIframe = null
    this.flowPopup = null
    this.cardIframe = null
  }

  async submitForm () {
    console.log('submitForm')
    if (this.form) {
      if (this.flowMode === 'iframe') {
        await this.insertFlowIframe()
      } else if (this.flowMode === 'popup') {
        this.openFlowPopup()
      }
      this.form.submit()
    }
  }

  showResult (data) {
    console.log('showResult')
    if (data) {
      document.querySelector('.result').textContent = JSON.stringify(data, null, '  ')
    }
  }

  removeResult () {
    console.log('removeResult')
    document.querySelector('.result').textContent = ''
  }

  getQueryParams (str) {
    str = str || ""
    let queryParams = {}
    if (str.length > 0) {
      let query = str.substr(1).split('&')
      for (let i = 0; i < query.length; i++) {
        let item = query[i].split('=')
        queryParams[item[0]] = decodeURIComponent(item[1])
      }
    }
    return queryParams
  }

  getCCType(number) {
    let type = null
    let allowedCCTypes = this.allowedCCTypes || []
    number = number.replace(/[ -]+/g, '')
    if (number.match(/^(4)/g)) {
      type = 'vis'
    } else {
      if (number.match(/^(5[0-5])/g)) {
        type = 'eca'
      } else {
        if (number.match(/^3[4|7]/g)) {
          type = 'amx'
        } else {
          if (number.match(/^36/g)) {
            type = 'din'
          } else {
            if (number.match(/^6((011)|5)/g)) {
              type = 'dis'
            } else {
              if (number.match(/^(5[06789]|6)[0-9]{0,}$/g)) {
                type = 'mae'
              } else {
                if (number.match(/^35/g)) {
                  type = 'jcb'
                }
              }
            }
          }
        }
      }
    }
    return allowedCCTypes.indexOf(type) >= 0 ? type : null
  }

  receiveMessage (event) {
    console.log('receiveMessage', event)
    if (!event.data || typeof event.data !== 'string') { return }

    console.log(event.origin)

    let topic, data
    try {
      let eventData = JSON.parse(event.data)
      topic = eventData.topic || null
      data = eventData.data || {}
    } catch (err) {}

    if (topic === null) { return }

    console.log(topic, data)

    switch(topic) {
      case 'rnw-creditcardform.token-received':
        // this.updateFormData({ payment_method: data.paymentMethod, card_token: data.token })
        this.updateFormData({ card_token: data.token })
        this.submitForm()
        break
      case  'rnw-creditcardform.data-changed':
        this.updateFormData({ payment_method: '', card_token: '' })
        break
      case 'rnw-creditcardform.form-not-valid':
        this.updateFormData({ payment_method: '', card_token: '' })
        break
      case 'rnw-creditcardform.create-token-error':
        this.updateFormData({ payment_method: '', card_token: '' })
        break
      case 'rnw-creditcardform.form-valid':
        this.postMessage(this.cardIframe.contentWindow, {topic: 'rnw-creditcardform.create-token', data: {apiKey: this.merchantApiKey}})
        break
      case 'flow-close-popup':
        this.removeForm()
        this.showResult(data)
        break
    }
  }

  postMessage (targetWin, data) {
    console.log('postMessage', data)
    targetWin.postMessage(JSON.stringify(data), '*')
  }

  updateFormData (data) {
    console.log('updateFormData', data)
    Object.keys(data).forEach((f) => {
      let input = document.querySelector(`.form-cont form input[name=${f}]`)
      if (input) {
        input.value = data[f]
      }
    })
  }
}

window.pp = new PaymentProcess({
  merchantApiKey: '1234567890',
  // flowMode: document.querySelector('.settings [name=flowMode]:checked').value,
  // cardMode: document.querySelector('.settings [name=cardMode]:checked').value,
  allowedCCTypes: ['vis', 'eca', 'amx'],
  fields: {
    success_url: window.location.href,
    error_url: window.location.href,
    cancel_url: window.location.href,
    test_mode: 'true',
    mobile_mode: 'false',
    amount: '8100',  // Authorized
    // amount: '9100',  // Declined
    // amount: '10100', // Referral
    // amount: '11100', // Card blocked
    currency: 'chf',
    language: 'en',
    reqtype: 'CAA',

    card_holder_name: 'Vasia Pupkin',

    payment_method: '',

    // card_token: '',

    // cardno: '',
    // cvv: '',
    // expm: '',
    // expy: '',
  },
})





