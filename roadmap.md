- check todos
- credit card token generation
- add all payment methods
- payment flow support
- validation rules for all payment parameters
- all payment methods in demo
- events for things like crdit-card-iframe render-start, render-end (interactions with sites), payment success, etc.

- solve lodash size problem
- minification of code
- refactoring

- tests
- translations 
- custom validation rules

- server side support
