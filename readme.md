- publish to window or import
- use namespaces
- secure config checks
- bugtracking
- communicate with developers trough events

```js
(async function() {
  //////////////////////////////////////////////////////////////////////////////
  // 1. Retrieve form data from your widget
  //////////////////////////////////////////////////////////////////////////////

  // return form data, you might want to
  // retrieve actual form data from an html form
  function getFormData() {
    return {
      // supported flows are iframe, popup or redirect
      flow: 'iframe',
      currency: 'usd',
      amount: 1000,
      cardno: '4242424242424242',
      cvv: '123',
      expm: '12',
      expy: '18',
      stored_customer_firstname: 'John',
      stored_customer_lastname: 'Snow',
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // 2. Handle validation errors
  //////////////////////////////////////////////////////////////////////////////

  // handle validation errors
  function handleValidationErrors(errors) {
    console.log('validation errors', errors)
  }

  //////////////////////////////////////////////////////////////////////////////
  // 3. Handle payment response
  //////////////////////////////////////////////////////////////////////////////

  // handle payment response, either successful or not
  function handleResponse(response) {
    let success = response.epayment_status === 'success'

    if (success) {
      console.log('successful payment', response)
    }

    if ( ! success) {
      console.error('failed payment', response)
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // 4. Submit a new payment
  //////////////////////////////////////////////////////////////////////////////

  async function sendPayment (payment) {
    // validate payment
    let errors = await payment.validate()

    // handle errors, if any
    if (errors) {
      handleValidationErrors(errors)
    }

    if ( ! errors) {
      // submit payment
      let response = await payment.send()
      handleResponse(response)
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // 5. Create a new EPIK instance
  //////////////////////////////////////////////////////////////////////////////

  // create a new epik instance
  let epik = rnw.epik.createEpik({
    // your api key
    apiKey: 1234567890,
    // use current window location for success / error / cancel payment redirects
    successUrl: window.location.origin + window.location.pathname + '?success',
    errorUrl: window.location.origin + window.location.pathname + '?error',
    cancelUrl: window.location.origin + window.location.pathname + '?cancel',
  })


  //////////////////////////////////////////////////////////////////////////////
  // 6. Actual code to trigger a payment
  //////////////////////////////////////////////////////////////////////////////

  // retrieve probable redirect response,
  // it might or might not exist
  // you'll get a redirect response after a payment
  // that has caused the browser to redirect away from your page
  let response = await epik.getRedirectPaymentResponse()

  // if there is a redirect payment response available, handle it
  if (response) {
    handleResponse(response)
  }

  if ( ! response) {
    // create a new payment based on form data
    let payment = epik.createPayment(getFormData())
    // set to true to enable credit card iframe
    let useCreditCardIframe = false

    // if credit card iframe is not used,
    // send payment immediately
    if ( ! useCreditCardIframe) {
      sendPayment(payment)
    }

    // if credit card iframe is used,
    // you may not send this payment immediately,
    // you must fill out the credit card form first
    if (useCreditCardIframe) {
      // render credit card iframe
      await payment.showCreditCardIframe()
      // call window.sendPayment() manually from console
      // after you've filled the credit card iframe
      window.sendPayment = () => sendPayment(payment)
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // YOU CAN COPY PASTE THIS INTO YOUR BROWSER'S CONSOLE
  //////////////////////////////////////////////////////////////////////////////
})()
```

## EPIK flow

Use this chart as reference for the various EPIK flows.

![flow chart](./flow.png)

# EPIK

E-Payment Integration Kit.

It's a JS-library that allows you to integrate Raisenow's E-payment functionalities into your website.

## Getting started

`npm install`

`npm run watch`

## Directory structure

`src` - Directory contains all `typescript` source files.

`build` - Directory contains built JS-bundles in various formats.

## Bundle formats

`es`, `cjs`, `umd`.

Default bundle format is `umd`. 

## Commands

All available command are in `package.json` file in `scripts` section:

`npm run build` - Build project

`npm run build:prod` - Build project and minify for production 

`npm run watch` - Watch source files for changes, build bundle in `umd` format and run a simple web-server for demo purposes

## todo: sample widget implementation flow
create epik
render widget
show redirect response
show form
submit form
validate payment
send payment
show response
