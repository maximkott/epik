import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import typescript from 'rollup-plugin-typescript2'
import uglify from 'rollup-plugin-uglify-es'
import gzip from 'rollup-plugin-gzip'
import filesize from 'rollup-plugin-filesize'
import typescriptCompiler from 'typescript'
import serve from 'serve'
import fs from 'fs'
import os from 'os'

////////////////////////////////////////////////////////////////////////////////

const ENV = (process.env.ENV || 'dev') // dev, prod
const FORMATS = (process.env.FORMATS || 'umd').split(',') // umd, cjs, es
const SOURCE = process.env.SOURCE || 'ts' // ts, js
const SOURCE_GROUPS = (process.env.GROUPS || 'epik,demo,credit-card').split(',') // epik, demo, credit-card
const SOURCE_DIR = SOURCE === 'ts' ? './src' : './build'
const SOURCE_FILE_TYPE = SOURCE === 'ts' ? 'ts' : 'js'
const TARGET = './build'
const PORT = process.env.PORT || 3456

let BUILD_CONFIG = {
  'epik': (group, format) => createConfig(group, format),
  'demo': (group, format) => {
    format === 'umd' && fs.writeFileSync('./build/demo.html', fs.readFileSync('./demo/demo.html'))

    return createConfig(group, format)
  },
  'credit-card': (group, format) => {
    format === 'umd' && fs.writeFileSync('./build/credit-card.html', fs.readFileSync('./demo/credit-card.html'))

    return createConfig(group, format)
  },
}

////////////////////////////////////////////////////////////////////////////////

if ( ! fs.existsSync(TARGET)){
  fs.mkdirSync(TARGET)
}

////////////////////////////////////////////////////////////////////////////////
let ROLLUP_CONFIG = []

SOURCE_GROUPS.forEach((group) => {
  FORMATS.forEach((format) => {
    ROLLUP_CONFIG.push(BUILD_CONFIG[group](group, format))
  })
})

export default ROLLUP_CONFIG

////////////////////////////////////////////////////////////////////////////////

if (process.env.ROLLUP_WATCH && ! process.env.ROLLUP_SERVER_STARTED) {
  let server = serve(TARGET, {port: PORT, clipless: true})

  process.on('exit', () => server.stop())
  process.on('SIGINT', () => server.stop())
  process.on('SIGUSR1', () => server.stop())
  process.on('SIGUSR2', () => server.stop())
  process.on('uncaughtException', () => server.stop())
  process.env.ROLLUP_SERVER_STARTED = true
}

////////////////////////////////////////////////////////////////////////////////

function createConfig (group, format) {
  let nameWithFormat = format === 'umd' ? group : group + '.' + format
  let inputFile = SOURCE_DIR + '/' + group + '/index.' + SOURCE_FILE_TYPE
  let outputFile = TARGET + '/' + nameWithFormat + (ENV === 'prod' && format === 'umd' ? '.min.js': '.js')
  let moduleName = 'rnw.' + group
  let sourceMaps = ENV !== 'prod'
  let tmpDir = os.tmpdir() + '/tscache'

  return {
    input: inputFile,
    sourcemap: sourceMaps,
      output: {
        file: outputFile,
        format: format,
        name: moduleName
    },
    plugins: [
      format === 'umd' ? nodeResolve() : {},
      commonjs({
        include: 'node_modules/**',
        sourceMap: sourceMaps
      }),
      SOURCE === 'ts' ? typescript({
        typescript: typescriptCompiler,
        cacheRoot: tmpDir
      }) : {},
      ENV === 'prod' && format === 'umd' ? uglify(): {},
      ENV === 'prod' && format === 'umd' ? gzip(): {},
      ENV === 'prod' && format === 'umd' ? filesize(): {}
    ]
  }
}
