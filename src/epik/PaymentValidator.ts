import {Epik} from './Epik'
import {PaymentErrors} from './PaymentErrors'
import {validateInRange, validateNumerical} from '../shared/genericValidation'
import {PaymentConfig} from './PaymentConfig'
import {
  isCreditCardPaymentMethod,
  isSupportedPaymentMethod, PaymentMethod
} from './PaymentMethod'
import {CreditCardIframe} from './CreditCardIframe'
import {
  validateCreditCardCvv, validateCreditCardExpirationMonth,
  validateCreditCardExpirationYear,
  validateCreditCardNumber
} from '../shared/creditCardValidation'
import {getCreditCardType} from '../shared/CreditCardType'
import {validateIBAN} from '../shared/ibanValidation'

export class PaymentValidator {
  constructor(private epik: Epik) {}

  validateMetadata(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    let hasSuccessUrl = !!payment.success_url
    let hasErrorUrl = !!payment.error_url
    let hasCancelUrl = !!payment.cancel_url
    let hasTestMode = !!payment.test_mode
    let isValidTestMode = validateInRange(payment.test_mode, ['true', 'false'])
    let hasMobileMode = !!payment.mobile_mode
    let isValidMobileMode = validateInRange(payment.mobile_mode, ['true', 'false'])
    let hasLanguage = !!payment.language
    let isValidLanguage = this.epik.config.languages.indexOf(payment.language) !== -1
    let hasReqtype = !!payment.reqtype
    let isValidReqtype = payment.reqtype === 'CAA'
    let hasFurtherRnwInteraction = !! payment.further_rnw_interaction
    let isValidFurtherRnwInteraction = validateInRange(payment.further_rnw_interaction, ['enabled', 'disabled'])

    if (!hasSuccessUrl) {
      errors.success_url = 'Missing success url setting'
    }

    if (!hasErrorUrl) {
      errors.error_url = 'Missing error url setting'
    }

    if (!hasCancelUrl) {
      errors.cancel_url = 'Missing cancel url setting'
    }

    if (!hasTestMode) {
      errors.test_mode = 'Missing test mode setting'
    } else if (!isValidTestMode) {
      errors.test_mode = 'Invalid value'
    }

    if (!hasMobileMode) {
      errors.mobile_mode = 'Missing mobile mode setting'
    } else if (!isValidMobileMode) {
      errors.mobile_mode = 'Invalid value'
    }

    if (!hasLanguage) {
      errors.language = 'Missing language setting'
    } else if ( ! isValidLanguage) {
      errors.language = 'Invalid value'
    }

    if (!hasReqtype) {
      errors.reqtype = 'Missing reqtype setting'
    } else if ( ! isValidReqtype) {
      errors.reqtype = 'Invalid value'
    }

    if (!hasFurtherRnwInteraction) {
      errors.further_rnw_interaction = 'Missing futher interaction setting'
    } else if ( ! isValidFurtherRnwInteraction) {
      errors.further_rnw_interaction = 'Invalid value'
    }

    return errors
  }

  async validateCreditCardIframe(payment: PaymentConfig, creditCardIframe: CreditCardIframe): Promise<PaymentErrors> {
    if (creditCardIframe) {
      return await this.epik.validateCreditCardIframe(creditCardIframe)
    }

    return {} as PaymentErrors
  }

  validatePaymentMethod(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    let hasPaymentMethod = !!payment.payment_method
    let isValidPaymentMethod = isSupportedPaymentMethod(payment.payment_method)

    if (!hasPaymentMethod) {
      errors.payment_method = 'Payment method missing'
    } else if (!isValidPaymentMethod) {
      errors.payment_method = 'Payment method ' + payment.payment_method + ' is not supported'
    }

    return errors
  }

  validateAmount(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    let hasAmount = !! payment.amount
    let isValidAmount = validateNumerical(payment.amount)

    if ( ! hasAmount) {
      errors.amount = 'Amount missing'
    } else if ( ! isValidAmount) {
      errors.amount = 'Invalid value'
    }

    return errors
  }

  validateCurrency(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    let hasCurrency = !!payment.currency
    let isValidCurrency = this.epik.config.currencies.indexOf(payment.currency) !== -1

    if (!hasCurrency) {
      errors.currency = 'Missing currency'
    } else if (!isValidCurrency) {
      errors.currency = 'Currency ' + payment.currency + ' is not valid'
    }

    return errors
  }

  validateCustomer(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    let hasStoredCustomerZipCode = !!payment.stored_customer_zip_code
    let isValidStoredCustomerZipCode = validateNumerical(payment.stored_customer_zip_code)

    if (hasStoredCustomerZipCode && !isValidStoredCustomerZipCode) {
      errors.stored_customer_zip_code = 'Invalid customer zip code'
    }

    return errors
  }

  validateCreditCard(payment: PaymentConfig, creditCardIframe: CreditCardIframe): PaymentErrors {
    let errors = {} as PaymentErrors

    if (isCreditCardPaymentMethod(payment.payment_method)) {
      let hasCardHolderName = !!payment.card_holder_name
      let hasFirstName = !!payment.stored_customer_firstname
      let hasLastName = !!payment.stored_customer_lastname
      let hasCardNumber = !!payment.cardno
      let hasCVV = !!payment.cvv
      let hasExpirationMonth = !!payment.expm
      let hasExpirationYear = !!payment.expy

      let isValidCardNumber = validateCreditCardNumber(payment.cardno)
      let isValidCVV = validateCreditCardCvv(payment.cvv)
      let isValidCardNumberAndPaymentMethodCombination = getCreditCardType(payment.cardno) === payment.payment_method // todo: check?
      let isValidExpirationMonth = validateCreditCardExpirationMonth(payment.expm)
      let isValidExpirationYear = validateCreditCardExpirationYear(payment.expy)

      if (!hasCardHolderName) {
        errors.card_holder_name = 'Missing credit card holder name'

        if ( ! hasFirstName) {
          errors.stored_customer_firstname = 'Missing customer first name'
        }

        if ( ! hasLastName) {
          errors.stored_customer_lastname = 'Missing customer last name'
        }
      }

      if (!creditCardIframe) {
        if (!hasCardNumber) {
          errors.cardno = 'Missing credit card number'
        } else if (!isValidCardNumber) {
          errors.cardno = 'Invalid credit card number'
        } else if (!isValidCardNumberAndPaymentMethodCombination) {
          errors.cardno = 'Credit card number does not match payment method ' + errors.payment_method
        }

        if (!hasCVV) {
          errors.cvv = 'Missing credit card cvv'
        } else if (!isValidCVV) {
          errors.cvv = 'Invalid credit card cvv'
        }

        if (!hasExpirationMonth) {
          errors.expm = 'Missing credit card expiration month'
        } else if (!isValidExpirationMonth) {
          errors.expm = 'Invalid credit card expiration month'
        }

        if (!hasExpirationYear) {
          errors.expy = 'Missing credit card expiration year'
        } else if (!isValidExpirationYear) {
          errors.expy = 'Invalid credit card expiration year'
        }
      }
    }

    return errors
  }

  validateDirectDebit(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    if (payment.payment_method === PaymentMethod.DIRECT_DEBIT) {
      let hasIban = !!payment.iban
      let hasBankName = !!payment.bank_name
      let hasBankZipCode = !!payment.bank_zip_code
      let hasBankCity = !!payment.bank_city

      let hasStoredCustomerFirstname = !!payment.stored_customer_firstname
      let hasStoredCustomerLastname = !!payment.stored_customer_lastname
      let hasStoredCustomerStreet = !!payment.stored_customer_street
      let hasStoredCustomerCity = !!payment.stored_customer_city
      let hasStoredCustomerZipCode = !!payment.stored_customer_zip_code

      let isValidIban = validateIBAN(payment.iban)
      let isValidBankZipCode = validateNumerical(payment.bank_zip_code)

      if (!hasStoredCustomerFirstname) {
        errors.stored_customer_firstname = 'Missing customer firstname'
      }

      if (!hasStoredCustomerLastname) {
        errors.stored_customer_lastname = 'Missing customer lastname'
      }

      if (!hasStoredCustomerStreet) {
        errors.stored_customer_street = 'Missing customer street'
      }

      if (!hasStoredCustomerCity) {
        errors.stored_customer_city = 'Missing customer city'
      }

      if (!hasStoredCustomerZipCode) {
        errors.stored_customer_zip_code = 'Missing customer zip code'
      }

      if (!hasIban) {
        errors.iban = 'Missing bank IBAN'
      } else if (!isValidIban) {
        errors.iban = 'Invalid bank IBAN'
      }

      if (!hasBankName) {
        errors.bank_name = 'Missing bank name'
      }

      if (!hasBankZipCode) {
        errors.bank_zip_code = 'Missing bank zip code'
      } else if (!isValidBankZipCode) {
        errors.bank_zip_code = 'Invalid bank zip code'
      }

      if (!hasBankCity) {
        errors.bank_city = 'Missing bank city'
      }
    }

    return errors
  }

  validateElv(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    if (payment.payment_method === PaymentMethod.ELV) {
      let hasBankiban = !!payment.bankiban

      let isValidBankiban = validateIBAN(payment.bankiban) // todo: check?

      if (!hasBankiban) {
        errors.bankiban = 'Missing bank IBAN'
      } else if (!isValidBankiban) {
        errors.bankiban = 'Invalid bank IBAN'
      }
    }

    return errors
  }

  validatePaymentSlipWithReferenceNumber(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    if (payment.payment_method === PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER) {
      let hasPayerLine1 = !!payment.payer_line1
      let hasPayerLine2 = !!payment.payer_line2
      let hasPayerLine3 = !!payment.payer_line3
      let hasPayerLine4 = !!payment.payer_line4
      let hasRefno = !!payment.refno

      if (!hasPayerLine1) {
        errors.payer_line1 = 'Missing payer line 1'
      }

      if (!hasPayerLine2) {
        errors.payer_line2 = 'Missing payer line 2'
      }

      if (!hasPayerLine3) {
        errors.payer_line3 = 'Missing payer line 3'
      }

      if (!hasPayerLine4) {
        errors.payer_line4 = 'Missing payer line 4'
      }

      if (!hasRefno) {
        errors.refno = 'Missing reference number'
      }
    }

    return errors
  }

  validateSepaOne(payment: PaymentConfig): PaymentErrors {
    let errors = {} as PaymentErrors

    if (payment.payment_method === PaymentMethod.SEPA_ONE) {
      // todo: check iban
    }

    return errors
  }
}
