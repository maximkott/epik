import {PaymentFlowType} from './PaymentFlowType'

export type EpikConfig = {
  epikId?: string
  apiKey?: string
  endpointUrl?: string
  creditCardIframeUrl?: string
  creditCardIframeCssUrl?: string
  preferredPaymentFlows?: PaymentFlowType[]
  errorUrl?: string
  successUrl?: string
  cancelUrl?: string
  testMode?: boolean
  mobileMode?: boolean
  language?: string
  languages?: string[]
  currencies?: string[]
  reqtype?: 'CAA'
  furtherRnwInteraction?: boolean
  creditCardIframeTargetSelector?: string
  paymentIframeTargetSelector?: string
}
