import qs from 'qs'
import {
  createAndInjectForm,
  createAndInjectIframe,
  fillForm
} from '../shared/Dom'
import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {IPaymentFlow} from './PaymentFlow'

export class PaymentFlowIframe implements IPaymentFlow {
  private iframe: HTMLIFrameElement

  async start(epik: Epik, payment: PaymentConfig, window: Window) {
    if ( ! this.iframe) {
      let onload = (iframe: HTMLIFrameElement) => {
        try {
          let data = qs.parse(iframe.contentWindow.location.search, {ignoreQueryPrefix: true})

          if (data.epayment_status) {
            this.iframe.parentNode.removeChild(this.iframe)
            this.iframe = null
          }
        } catch(err) {}
      }

      this.iframe = await createAndInjectIframe(window.document, {
        name: 'iframe-' + Date.now().toString(),
        hidden: false,
        onload: onload
      }, epik.config.paymentIframeTargetSelector)
    }

    let form = createAndInjectForm(window.document, {
      target: this.iframe.name,
      method: 'post',
      action: epik.getPaymentActionUrl(),
      hidden: true
    })

    fillForm(window.document, form, payment)

    form.submit()
  }
}
