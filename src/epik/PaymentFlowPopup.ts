import {createAndInjectForm, createAndOpenPopup, fillForm} from '../shared/Dom'
import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {IPaymentFlow} from './PaymentFlow'

export class PaymentFlowPopup implements IPaymentFlow {
  private popup: Window

  async start(epik: Epik, payment: PaymentConfig, window: Window) {
    if ( ! this.popup || this.popup.closed) {
      this.popup = await createAndOpenPopup(window, {
        name: 'popup-' + Date.now().toString(),
        features: 'width=400,height=400,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes',
      })
    } else {
      this.popup.focus()
    }

    let form = createAndInjectForm(window.document, {
      target: this.popup.name,
      method: 'post',
      action: epik.getPaymentActionUrl(),
      hidden: true
    })

    fillForm(window.document, form, payment)

    form.submit()
  }
}
