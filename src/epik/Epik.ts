import qs from 'qs'
import {CreditCardIframeMessageType} from '../shared/CreditCardIframeMessageType'
import {CreditCardType, getCreditCardType} from '../shared/CreditCardType'
import {validateCreditCardNumber} from '../shared/creditCardValidation'
import {createAndInjectIframe} from '../shared/Dom'
import {formatIBAN, validateIBAN} from '../shared/ibanValidation'
import {clone, endsWith, isArray, keys, startsWith} from '../shared/lodash'
import {Messenger} from '../shared/Messenger'
import {CreditCardIframe} from './CreditCardIframe'
import {EpikConfig} from './EpikConfig'
import {createPayment, Payment} from './Payment'
import {PaymentConfig} from './PaymentConfig'
import {PaymentErrors} from './PaymentErrors'
import {createPaymentFlow, IPaymentFlow} from './PaymentFlow'
import {
  getSupportedPaymentFlowTypeByPreference,
  PaymentFlowType
} from './PaymentFlowType'
import {PaymentPreparator} from './PaymentPreparator'
import {createPaymentResponse, PaymentResponse} from './PaymentResponse'
import {PaymentValidator} from './PaymentValidator'

let epikCounter = -1

export class Epik {
  private window: Window
  private redirectPaymentResponse: any
  private waitingForPaymentResponse: {[key: string]: {
    payment: PaymentConfig
    resolve: (value?: any) => void
    reject: (reason?: any) => void
  }}

  constructor(
    public config: EpikConfig,
    win?: Window
  ) {
    this.window = win || window
    this.redirectPaymentResponse = null
    this.waitingForPaymentResponse = {}

    this.handlePaymentResponses()
  }

  clone(overrides?: EpikConfig): Epik {
    overrides = overrides || overrides as EpikConfig

    let config = clone(this.config)
    config.epikId = overrides.epikId || (++epikCounter).toString()

    return createEpik({...config, ...overrides})
  }

  createPayment(payment?: PaymentConfig): Payment {
    return createPayment(this, payment)
  }

  getCreditCardType(number: string): CreditCardType {
    return getCreditCardType(number)
  }

  validateCreditCardNumber(cardNumber: string): boolean {
    return validateCreditCardNumber(cardNumber)
  }

  validateIBAN(iban: string): boolean {
    return validateIBAN(iban)
  }

  formatIBAN(iban: string): string {
    return formatIBAN(iban)
  }

  getPaymentActionUrl(): string {
    return this.config.endpointUrl + '/epayment/api/step/pay/merchant/' + this.config.apiKey
  }

  async createCreditCardIframe(targetSelector?: string): Promise<CreditCardIframe> {
    targetSelector = targetSelector || this.config.creditCardIframeTargetSelector

    let options = {
      src: this.config.creditCardIframeUrl,
      name: 'credit-card-iframe'
    }

    let iframe = await createAndInjectIframe(this.window.document, options, targetSelector)
    let messenger = new Messenger(window, iframe.contentWindow, 'master')
    await messenger.establishLinkBetweenSlaveAndMaster()

    return {iframe, messenger}
  }

  async validateCreditCardIframe(creditCardIframe: CreditCardIframe): Promise<PaymentErrors> {
    return creditCardIframe.messenger.request(CreditCardIframeMessageType.VALIDATE)
  }

  async acquireCreditCardTokenFromIframe(creditCardIframe: CreditCardIframe): Promise<string> {
    return creditCardIframe.messenger.request(CreditCardIframeMessageType.TOKENIZE)
  }

  async acquireCreditCardTypeFromIframe(creditCardIframe: CreditCardIframe): Promise<string> {
    return creditCardIframe.messenger.request(CreditCardIframeMessageType.DETECT_CREDIT_CARD_TYPE)
  }

  public async hasRedirectPaymentResponse(): Promise<boolean> {
    return await this.getRedirectPaymentResponse() !== null
  }

  public async getRedirectPaymentResponse(): Promise<PaymentResponse | null> {
    return this.redirectPaymentResponse
  }

  async sendPayment(payment: PaymentConfig, creditCardIframe?: CreditCardIframe): Promise<PaymentResponse> {
    return new Promise<PaymentResponse>(async (resolve, reject) => {
      payment = await this.preparePayment(payment, creditCardIframe)

      let paymentFlow = this.getPaymentFlow(payment)
      paymentFlow.start(this, payment, this.window)

      this.waitingForPaymentResponse[payment.stored_epik_payment_id] = {
        payment, resolve, reject
      }
    })
  }

  async validatePayment(payment: PaymentConfig, creditCardIframe?: CreditCardIframe): Promise<PaymentErrors | null> {
    payment = await this.preparePayment(payment, creditCardIframe)

    console.debug('validating payment', payment)

    let validator = new PaymentValidator(this)

    let errors = {
      ...validator.validateMetadata(payment),
      ...await validator.validateCreditCardIframe(payment, creditCardIframe),
      ...validator.validatePaymentMethod(payment),
      ...validator.validateAmount(payment),
      ...validator.validateCurrency(payment),
      ...validator.validateCustomer(payment),
      ...validator.validateCreditCard(payment, creditCardIframe),
      ...validator.validateDirectDebit(payment),
      ...validator.validateElv(payment),
      ...validator.validatePaymentSlipWithReferenceNumber(payment),
      ...validator.validateSepaOne(payment),
    } as PaymentErrors

    return keys(errors).length > 0 ? errors : null
  }

  private async preparePayment(payment: PaymentConfig, creditCardIframe?: CreditCardIframe): Promise<PaymentConfig> {
    payment = clone(payment) as PaymentConfig

    console.debug('preparing payment', payment)

    let preparator = new PaymentPreparator(this)

    let relevantFields = [
      ...preparator.prepareMetadata(payment),
      ...await preparator.prepareCreditCardIframe(payment, creditCardIframe),
      ...preparator.prepareCreditCard(payment, creditCardIframe),
      ...preparator.prepareDirectDebit(payment),
      ...preparator.prepareElv(payment),
      ...preparator.preparePaymentSlipWithReferenceNumber(payment),
      ...preparator.prepareSepaOne(payment),
    ]

    return this.pickPaymentFields(payment, relevantFields)
  }

  private pickPaymentFields(payment: PaymentConfig, fields: string[]): PaymentConfig {
    let data = {} as PaymentConfig

    fields.forEach((field) => {
      data[field] = payment[field]
    })

    keys(payment).forEach((field) => {
      if (startsWith(field, 'stored_')) {
        data[field] = payment[field]
      }
    })

    return data
  }

  public generatePaymentId(): string {
    if (this.hasId()) {
      return Date.now() + ':' + this.config.epikId
    }

    return Date.now().toString()
  }

  private isMyPaymentId(paymentId: string): boolean {
    if (this.hasId()) {
      return endsWith(paymentId, ':' + this.config.epikId)
    }

    if ( ! this.hasId() && ! paymentId) {
      return true
    }

    return paymentId.indexOf(':') === -1
  }

  private hasId(): boolean {
    return this.config.epikId !== '0'
  }

  private handlePaymentResponses() {
    let messenger = new Messenger(this.window)
    messenger.on('epik-payment-response', (data: PaymentResponse) => this.handlePaymentResponse(data))

    // catch redirect feedback
    let data = qs.parse(this.window.location.search, {ignoreQueryPrefix: true})
    let status = isArray(data.epayment_status) ? data.epayment_status[0] : data.epayment_status

    if (status) {
      if (this.window.opener) {
        // feedback trough popup
        data.flow = 'popup'

        let messenger = new Messenger(null, this.window.opener)
        messenger.emit('epik-payment-response', data)

        this.window.close()
      } else if (this.window.self !== this.window.top) {
        // feedback trough iframe
        data.flow = 'iframe'

        let messenger = new Messenger(null, this.window.top)
        messenger.emit('epik-payment-response', data)
      } else {
        // feedback trough redirect
        data.flow = 'redirect'
        this.handlePaymentResponse(data)
      }
    }
  }

  private handlePaymentResponse(data: PaymentResponse) {
    let response = createPaymentResponse(data)
    let paymentId = isArray(response.stored_epik_payment_id) ? response.stored_epik_payment_id[0] : response.stored_epik_payment_id

    let isRedirectResponse = response.flow === PaymentFlowType.REDIRECT
    let isMyRedirectResponse = this.isMyPaymentId(paymentId)

    if (isRedirectResponse && isMyRedirectResponse) {
      this.redirectPaymentResponse = response
    }

    let waitingForResponse = this.waitingForPaymentResponse[paymentId]

    if (waitingForResponse) {
      if (response.epayment_status === 'success') {
        waitingForResponse.resolve(response)
      } else {
        waitingForResponse.resolve(response)
      }
    }
  }

  private getPaymentFlow(payment: PaymentConfig): IPaymentFlow {
    let paymentFlows = clone(this.config.preferredPaymentFlows)

    if (payment.flow) {
      paymentFlows.unshift(payment.flow)
    }

    // allow to override payment flow
    if (payment.flow) {
      paymentFlows.unshift(payment.flow)
      delete payment.flow
    }

    let paymentFlowType = getSupportedPaymentFlowTypeByPreference(payment.payment_method, paymentFlows)
    let paymentFlow = createPaymentFlow(paymentFlowType)

    return paymentFlow
  }
}

export function createEpik(config: EpikConfig, win?: Window): Epik {
  win = win || window

  config = config || {} as EpikConfig
  config.epikId = config.epikId || (++epikCounter).toString()
  config.endpointUrl = config.endpointUrl || 'https://api.raisenow.com'
  config.apiKey = config.apiKey || '1234567890'
  config.creditCardIframeUrl = './credit-card.html?apiKey=' + config.apiKey + '&css_url=' + config.creditCardIframeCssUrl
  config.preferredPaymentFlows = config.preferredPaymentFlows || []
  config.successUrl = config.successUrl || win.location.origin + win.location.pathname + '?success'
  config.errorUrl = config.errorUrl || win.location.origin + win.location.pathname + '?error'
  config.cancelUrl = config.cancelUrl || win.location.origin + win.location.pathname + '?cancel'
  config.testMode = config.testMode === false ? false : true
  config.mobileMode = config.mobileMode === true ? true : false
  config.reqtype = config.reqtype || 'CAA'
  config.furtherRnwInteraction = config.furtherRnwInteraction === true ? true : false
  config.currencies = ['chf', 'usd', 'eur', 'gbp']
  config.language = config.language || 'en'
  config.languages = ['en', 'de', 'fr', 'it']

  return new Epik(config, win)
}
