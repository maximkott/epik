import {Epik} from './Epik'
import {PaymentFlowIframe} from './PaymentFlowIframe'
import {PaymentConfig} from './PaymentConfig'
import {PaymentFlowType} from './PaymentFlowType'
import {PaymentFlowPopup} from './PaymentFlowPopup'
import {PaymentFlowRedirect} from './PaymentFlowRedirect'

export interface IPaymentFlow {
  start(epik: Epik, payment: PaymentConfig, window: Window): void
}

export function createPaymentFlow (paymentFlowType: PaymentFlowType): IPaymentFlow {
  if (paymentFlowType === PaymentFlowType.REDIRECT) {
    return new PaymentFlowRedirect()
  } else if (paymentFlowType === PaymentFlowType.IFRAME) {
    return new PaymentFlowIframe()
  } else if (paymentFlowType === PaymentFlowType.POPUP) {
    return new PaymentFlowPopup()
  }

  throw new Error('Payment flow type "' + paymentFlowType + '" is not supported')
}
