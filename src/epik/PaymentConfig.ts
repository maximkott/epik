import {PaymentFlowType} from './PaymentFlowType'

export type PaymentConfig = {
  // epik
  flow?: PaymentFlowType
  stored_epik_payment_id?: string

  // general
  amount: any // number
  currency: any // "chf" or "usd" or "eur" or "gbp"
  payment_method: any // one of the payment methods

  // customer
  stored_customer_title?: any // string
  stored_customer_email?: any // string
  stored_customer_firstname?: any // string
  stored_customer_lastname?: any // string
  stored_customer_street?: any // string
  stored_customer_city?: any // string
  stored_customer_zip_code?: any // string
  stored_customer_country?: any // string

  // meta
  success_url?: any // string
  error_url?: any // string
  cancel_url?: any // string
  language?: any // "de" or "en" or "fr" or "it"
  test_mode?: any // "true" or "false"
  mobile_mode?: any // "true" or "false"
  reqtype?: any // "CAA"
  immediate_execution?: any // "true" or "false"
  further_rnw_interaction?: any

  // subscriptions
  recurring?: any // "true" or "false"
  recurring_interval?: any // string

  // credit cards
  cardno?: any // valid card number
  expm?: any // 2 digits number
  expy?: any // string
  cvv?: any // 3 digits number
  card_token?: any // string
  card_holder_name?: any // string

  // sms
  short_number?: any // number
  msisdn?: any // number

  // payment slip
  refno?: any // number

  // bank
  bank_name?: any // string
  bank_zip_code?: any // string
  bank_city?: any // string
  bankiban?: any // valid iban
  bankbic?: any // valid bic
  iban?: any // valid iban

  // misc
  campaign_id?: any // string
  campaign_subid?: any // string
  payer_line1?: any // string
  payer_line2?: any // string
  payer_line3?: any // string
  payer_line4?: any // string
}
