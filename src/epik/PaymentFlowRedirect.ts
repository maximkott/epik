import {createAndInjectForm, fillForm} from '../shared/Dom'
import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {IPaymentFlow} from './PaymentFlow'

export class PaymentFlowRedirect implements IPaymentFlow {
  start(epik: Epik, payment: PaymentConfig, window: Window): void {
    let form = createAndInjectForm(window.document, {
      target: '_self',
      method: 'post',
      action: epik.getPaymentActionUrl(),
      hidden: true
    })

    fillForm(window.document, form, payment)

    form.submit()
  }
}
