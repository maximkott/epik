export enum PaymentType {
  ONE_TIME = 'one-time',
  RECURRING = 'recurring',
}
