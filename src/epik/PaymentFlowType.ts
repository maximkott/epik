import {includes, intersection} from '../shared/lodash'
import {isSupportedPaymentMethod, PaymentMethod} from './PaymentMethod'

export enum PaymentFlowType {
  REDIRECT = 'redirect',
  IFRAME = 'iframe',
  POPUP = 'popup'
}

export const RedirectFlowPaymentMethods = [
  PaymentMethod.VISA,
  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.CYMBA,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DIRECT_DEBIT,
  PaymentMethod.DISCOVERY,
  PaymentMethod.EBS,
  PaymentMethod.ELV,
  PaymentMethod.EPS,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.VISA,
  PaymentMethod.MASTER_PASS,
  PaymentMethod.PAYMENT_SLIP,
  PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
  PaymentMethod.PAY_PAL,
  PaymentMethod.POST_FINANCE_CARD,
  PaymentMethod.POST_FINANCE,
  PaymentMethod.SEPA_ONE,
  PaymentMethod.SOFORT,
  PaymentMethod.STRIPE,
  PaymentMethod.SMS,
  PaymentMethod.TWINT,
]

export const IframeFlowPaymentMethods = [
  PaymentMethod.VISA,
  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.CYMBA,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DIRECT_DEBIT,
  PaymentMethod.DISCOVERY,
  PaymentMethod.EBS,
  PaymentMethod.ELV,
  PaymentMethod.EPS,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.VISA,
  PaymentMethod.MASTER_PASS,
  PaymentMethod.PAYMENT_SLIP,
  PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
  PaymentMethod.PAY_PAL,
  PaymentMethod.POST_FINANCE_CARD,
  PaymentMethod.POST_FINANCE,
  PaymentMethod.SEPA_ONE,
  PaymentMethod.SOFORT,
  PaymentMethod.STRIPE,
  PaymentMethod.SMS,
  PaymentMethod.TWINT,
]

export const PopupFlowPaymentMethods = [
  PaymentMethod.VISA,
  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.CYMBA,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DIRECT_DEBIT,
  PaymentMethod.DISCOVERY,
  PaymentMethod.EBS,
  PaymentMethod.ELV,
  PaymentMethod.EPS,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.VISA,
  PaymentMethod.MASTER_PASS,
  PaymentMethod.PAYMENT_SLIP,
  PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
  PaymentMethod.PAY_PAL,
  PaymentMethod.POST_FINANCE_CARD,
  PaymentMethod.POST_FINANCE,
  PaymentMethod.SEPA_ONE,
  PaymentMethod.SOFORT,
  PaymentMethod.STRIPE,
  PaymentMethod.SMS,
  PaymentMethod.TWINT,
]

export function getSupportedPaymentFlowTypes (paymentMethod: PaymentMethod): PaymentFlowType[] {
  if ( ! isSupportedPaymentMethod(paymentMethod)) {
    throw new Error('Payment method "' + paymentMethod + '" is not supported')
  }

  let flowTypes = []

  if (includes(RedirectFlowPaymentMethods, paymentMethod)) {
    flowTypes.push(PaymentFlowType.REDIRECT)
  }

  if (includes(IframeFlowPaymentMethods, paymentMethod)) {
    flowTypes.push(PaymentFlowType.IFRAME)
  }

  if (includes(PopupFlowPaymentMethods, paymentMethod)) {
    flowTypes.push(PaymentFlowType.POPUP)
  }

  if (flowTypes.length === 0) {
    // every payment method supports redirects by default
    flowTypes.push(PaymentFlowType.REDIRECT)
  }

  return flowTypes
}

export function isPaymentFlowTypeSupported (paymentMethod: PaymentMethod, paymentFlowType: PaymentFlowType) {
  let flowTypes = getSupportedPaymentFlowTypes(paymentMethod)

  if ( ! includes(flowTypes, paymentFlowType)) {
    return false
  }

  return true
}

export function getSupportedPaymentFlowTypeByPreference (paymentMethod: PaymentMethod, preferredPaymentFlowTypes: PaymentFlowType[]) {
  preferredPaymentFlowTypes = preferredPaymentFlowTypes || []

  let supportedPaymentFlowTypes = getSupportedPaymentFlowTypes(paymentMethod)
  let supportedAndPreferredPaymentFlowTypes = intersection(preferredPaymentFlowTypes, supportedPaymentFlowTypes)

  return supportedAndPreferredPaymentFlowTypes[0] || supportedPaymentFlowTypes[0]
}
