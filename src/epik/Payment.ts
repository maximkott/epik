import {merge} from '../shared/lodash'
import {CreditCardIframe} from './CreditCardIframe'
import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {PaymentErrors} from './PaymentErrors'
import {PaymentResponse} from './PaymentResponse'

export class Payment {
  private creditCardIframe: CreditCardIframe

  constructor(
    private epik: Epik,
    private config: PaymentConfig = {} as PaymentConfig
  ) {
  }

  update(config: PaymentConfig) {
    this.config = merge(this.config, config)
  }

  async send(): Promise<PaymentResponse> {
    return this.epik.sendPayment(this.config, this.creditCardIframe)
  }

  async validate(): Promise<PaymentErrors> {
    return this.epik.validatePayment(this.config, this.creditCardIframe)
  }

  async showCreditCardIframe(targetSelector?: string): Promise<CreditCardIframe> {
    if ( ! this.creditCardIframe) {
      this.creditCardIframe = await this.epik.createCreditCardIframe(targetSelector)
    }

    return this.creditCardIframe
  }

  hideCreditCardIframe() {
    if (this.creditCardIframe) {
      this.creditCardIframe.iframe.parentNode.removeChild(this.creditCardIframe.iframe)
      this.creditCardIframe = null
    }
  }
}

export function createPayment (epik: Epik, payment?: PaymentConfig): Payment {
  return new Payment(epik, payment)
}
