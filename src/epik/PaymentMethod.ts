import {includes} from '../shared/lodash'

export enum PaymentMethod {
  AMERICAN_EXPRESS = 'amx',
  CYMBA = 'cym',
  DINERS_CLUB = 'din',
  DIRECT_DEBIT = 'dd',
  DISCOVERY = 'dis',
  EBS = 'ebs',
  ELV = 'elv',
  EPS = 'eps',
  MASTER_CARD = 'eca',
  VISA = 'vis',
  MASTER_PASS = 'mpw',
  PAYMENT_SLIP = 'es',
  PAYMENT_SLIP_WITH_REFERENCE_NUMBER = 'ezs',
  PAY_PAL = 'pp',
  POST_FINANCE_CARD = 'pfc',
  POST_FINANCE = 'pef',
  SEPA_ONE = 'sod',
  SOFORT = 'dib',
  STRIPE = 'stp',
  SMS = 'sms',
  TWINT = 'twi',
}

export const SupportedPaymentMethods = [
  PaymentMethod.EBS,
  PaymentMethod.ELV,
  PaymentMethod.EPS,
  PaymentMethod.PAYMENT_SLIP,
  PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
  PaymentMethod.POST_FINANCE,
  PaymentMethod.SOFORT,
  PaymentMethod.TWINT,

  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.CYMBA,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DIRECT_DEBIT,
  PaymentMethod.DISCOVERY,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.MASTER_PASS,
  PaymentMethod.PAY_PAL,
  PaymentMethod.POST_FINANCE_CARD,
  PaymentMethod.SEPA_ONE,
  PaymentMethod.STRIPE,
  PaymentMethod.SMS,
  PaymentMethod.VISA,
]

export const OneTimePaymentMethods = [
  PaymentMethod.EBS,
  PaymentMethod.ELV,
  PaymentMethod.EPS,
  PaymentMethod.PAYMENT_SLIP,
  PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
  PaymentMethod.POST_FINANCE,
  PaymentMethod.SOFORT,
  PaymentMethod.TWINT,
]

export const RecurringPaymentMethods = [
  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.CYMBA,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DIRECT_DEBIT,
  PaymentMethod.DISCOVERY,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.MASTER_PASS,
  PaymentMethod.PAY_PAL,
  PaymentMethod.POST_FINANCE_CARD,
  PaymentMethod.SEPA_ONE,
  PaymentMethod.STRIPE,
  PaymentMethod.SMS,
  PaymentMethod.VISA,
]

export const CreditCardPaymentMethods = [
  PaymentMethod.VISA,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.DISCOVERY,
  PaymentMethod.AMERICAN_EXPRESS
]

export function isSupportedPaymentMethod (paymentMethod: string|PaymentMethod) {
  return includes(SupportedPaymentMethods, paymentMethod)
}

export function isOneTimePaymentMethod (paymentMethod: PaymentMethod): boolean {
  return includes(OneTimePaymentMethods, paymentMethod)
}

export function isRecurringPaymentMethod (paymentMethod: PaymentMethod): boolean {
  return includes(RecurringPaymentMethods, paymentMethod)
}

export function isCreditCardPaymentMethod (paymentMethod: PaymentMethod) {
  return includes(CreditCardPaymentMethods, paymentMethod)
}
