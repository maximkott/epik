import {PaymentConfig} from './PaymentConfig'
import {PaymentFlowType} from './PaymentFlowType'

export type PaymentResponse = PaymentConfig & {
  flow: PaymentFlowType
  epayment_status: 'success' | 'error' | 'cancel'
}

export function createPaymentResponse (data: any): any {
  return data
}
