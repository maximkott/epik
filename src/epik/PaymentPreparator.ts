import {PaymentConfig} from './PaymentConfig'
import {Epik} from './Epik'
import {CreditCardIframe} from './CreditCardIframe'
import {getCreditCardType} from '../shared/CreditCardType'
import {isCreditCardPaymentMethod, PaymentMethod} from './PaymentMethod'

export class PaymentPreparator {
  constructor(private epik: Epik) {}

  prepareMetadata(payment: PaymentConfig): string[] {
    let relevantFields = [
      'flow',
      'payment_method',
      'currency',
      'amount',
      'stored_epik_payment_id',
      'success_url',
      'error_url',
      'cancel_url',
      'test_mode',
      'mobile_mode',
      'language',
      'reqtype',
      'further_rnw_interaction',
    ]

    payment.stored_epik_payment_id = this.epik.generatePaymentId()
    payment.success_url = payment.success_url || this.epik.config.successUrl
    payment.error_url = payment.error_url || this.epik.config.errorUrl
    payment.cancel_url = payment.cancel_url || this.epik.config.cancelUrl
    payment.test_mode = payment.test_mode || this.epik.config.testMode ? 'true' : 'false'
    payment.mobile_mode = payment.mobile_mode || this.epik.config.mobileMode ? 'true' : 'false'
    payment.language = payment.language || this.epik.config.language
    payment.reqtype = payment.reqtype || this.epik.config.reqtype
    payment.further_rnw_interaction = payment.further_rnw_interaction || this.epik.config.furtherRnwInteraction ? 'enabled' : 'disabled'

    return relevantFields
  }

  async prepareCreditCardIframe(payment: PaymentConfig, creditCardIframe: CreditCardIframe): Promise<string[]> {
    let relevantFields = []

    if (creditCardIframe) {
      relevantFields.push('card_token')

      let paymentMethod = await this.epik.acquireCreditCardTypeFromIframe(creditCardIframe)

      if (paymentMethod) {
        payment.payment_method = paymentMethod

        let cardToken = await this.epik.acquireCreditCardTokenFromIframe(creditCardIframe)
        payment.card_token = cardToken
      } else {
        payment.payment_method = 'vis'
      }
    }

    return relevantFields
  }

  prepareCreditCard(payment: PaymentConfig, creditCardIframe: CreditCardIframe): string[] {
    let relevantFields = []

    let hasCardNumberWithoutPaymentMethod = !! payment.cardno && ! payment.payment_method

    if (hasCardNumberWithoutPaymentMethod) {
      payment.payment_method = getCreditCardType(payment.cardno)
    }

    let isCreditCard = isCreditCardPaymentMethod(payment.payment_method)

    if (isCreditCard) {
      relevantFields.push('card_holder_name')

      let hasCardHolderName = !!payment.card_holder_name
      let hasFirstAndLastName = !!payment.stored_customer_firstname && !!payment.stored_customer_lastname

      if (!hasCardHolderName && hasFirstAndLastName) {
        payment.card_holder_name = payment.stored_customer_firstname + ' ' + payment.stored_customer_lastname
      }

      if ( ! creditCardIframe) {
        relevantFields.push('cardno', 'expm', 'expy', 'cvv', 'card_holder_name')

        let hasCardNumber = !!payment.cardno
        let hasPaymentMethod = !!payment.payment_method
        let isValidCreditCardType = getCreditCardType(payment.cardno) === payment.payment_method
        let hasCardNumberWithoutPaymentMethod = hasCardNumber && !hasPaymentMethod
        let needsCreditCardType = hasCardNumberWithoutPaymentMethod || hasCardNumber && ! isValidCreditCardType

        if (hasCardNumber) {
          payment.cardno = payment.cardno.replace(/[A-z\s-]+/g, '')
        }

        if (needsCreditCardType) {
          payment.payment_method = getCreditCardType(payment.cardno)
        }
      }
    }

    return relevantFields
  }

  prepareDirectDebit(payment: PaymentConfig): string[] {
    let relevantFields = []
    let isDirectDebit = payment.payment_method === PaymentMethod.DIRECT_DEBIT

    if (isDirectDebit) {
      relevantFields.push('iban', 'bank_name', 'bank_city', 'bank_zip_code')
    }

    return relevantFields
  }

  prepareElv(payment: PaymentConfig): string[] {
    let relevantFields = []
    let isElv = payment.payment_method === PaymentMethod.ELV

    if (isElv) {
      relevantFields.push('bankiban')
    }

    return relevantFields
  }

  preparePaymentSlipWithReferenceNumber(payment: PaymentConfig): string[] {
    let relevantFields = []
    let isEzs = payment.payment_method === PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER

    if (isEzs) {
      relevantFields.push('payer_line1', 'payer_line2', 'payer_line3', 'payer_line4', 'refno')
    }

    return relevantFields
  }

  prepareSepaOne(payment: PaymentConfig): string[] {
    let relevantFields = []
    let isSepaOne = payment.payment_method === PaymentMethod.SEPA_ONE

    if (isSepaOne) {
      // todo: check iban
      // relevantFields.push('iban')
    }

    return relevantFields
  }
}
