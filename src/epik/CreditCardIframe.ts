import {Messenger} from '../shared/Messenger'

export type CreditCardIframe = {
  iframe: HTMLIFrameElement
  messenger: Messenger
}
