import {Epik} from '../epik/Epik'
import {Payment} from '../epik/Payment'
import {PaymentErrors} from '../epik/PaymentErrors'
import {PaymentResponse} from '../epik/PaymentResponse'
import {findElement} from '../shared/Dom'
import {each} from '../shared/lodash'

function createH1 (text) {
  let h1 = document.createElement('h1')
  h1.innerText = text

  return h1
}

function createForm(children: HTMLElement[] = []): HTMLFormElement {
  let form = document.createElement('form')

  children.forEach((c: HTMLElement) => form.appendChild(c))

  return form
}

function createSelect(name: string, options: HTMLOptionElement[] = [], placeholder?: string): HTMLLabelElement {
  let select = document.createElement('select')
  select.name = name

  options.forEach((o: HTMLOptionElement) => select.appendChild(o))

  return createLabel(placeholder, [select])
}

function createOption(value, text): HTMLOptionElement {
  let option = document.createElement('option')
  option.value = value
  option.text = text

  return option
}

function createInput(name, value, placeholder = ''): HTMLLabelElement {
  let input = document.createElement('input')
  input.type = 'text'
  input.name = name
  input.value = value
  input.placeholder = placeholder

  return createLabel(placeholder, [input])
}

function createSubmit(text): HTMLButtonElement {
  let button = document.createElement('button')
  button.innerText = text
  button.type = 'submit'

  return button
}

function createBreak(): HTMLDivElement {
  let div = document.createElement('div')
  div.appendChild(document.createElement('br'))

  return div
}

function createCheckbox(name, checked, text): HTMLLabelElement {
  let label = document.createElement('label')
  let checkbox = document.createElement('input')
  checkbox.type = 'checkbox'
  checkbox.name = name
  checkbox.checked = checked
  let description = document.createTextNode(text)

  label.appendChild(checkbox)
  label.appendChild(description)

  return label
}

function createDiv(id?, cls?, children: HTMLElement[] = []) {
  let div = document.createElement('div')

  if (id) {
    div.id = id.replace('#', '')
  }

  if (cls) {
    div.className = cls
  }

  children.forEach((c: HTMLElement) => div.appendChild(c))

  return div
}

function createLabel (text: string, children: HTMLElement[] = []): HTMLLabelElement {
  let label = document.createElement('label')

  if (text) {
    let title = document.createElement('span')
    title.innerHTML = text + '<br>'
    children.unshift(title)
  }

  children.forEach((c: HTMLElement) => label.appendChild(c))

  return label
}

export class DemoWidget {
  private payment: Payment
  private response: PaymentResponse
  private errors: PaymentErrors
  private form: HTMLFormElement

  constructor(
    private targetSelector: string,
    private epik: Epik
  ) {
    this.setup()
  }

  private async setup() {
    this.payment = this.epik.createPayment()
    this.form = this.createForm(this.targetSelector)

    this.dataChanged()

    let response = await this.epik.getRedirectPaymentResponse()

    if (response) {
      this.handlePaymentResponse(response)
    }
  }

  private createForm(targetSelector: string): HTMLFormElement {
    let form = createForm([
      createH1('Widget #' + targetSelector.substr(-1)),
      createCheckbox('test_mode', true, 'Test Mode'),
      createCheckbox('credit-card-iframe', false, 'Credit card iframe'),
      createBreak(),
      createSelect('flow', [
        createOption('iframe', 'IFRAME'),
        createOption('popup', 'POPUP'),
        createOption('redirect', 'REDIRECT')
      ], 'Payment flow'),
      createSelect('payment_method', [
        createOption('vis', 'Credit Card'),
        createOption('dd', 'Direct Debit'),
        createOption('ebs', 'EBS (India only)'),
        createOption('elv', 'ELV'),
        createOption('eps', 'EPS (Austria only)'),
        createOption('mpw', 'MasterPass'),
        createOption('es', 'Payment Slip'),
        createOption('ezs', 'Payment Slip (RefNo)'),
        createOption('pp', 'PayPal'),
        createOption('pfc', 'PostFinance Card'),
        createOption('pef', 'PostFinance E-Finance'),
        createOption('sod', 'SEPAone'),
      ], 'Payment method'),
      createSelect('currency', [
        createOption('chf', 'CHF'),
        createOption('eur', 'EUR'),
        createOption('usd', 'USD')
      ], 'Currency'),
      createSelect('language', [
        createOption('en', 'English'),
        createOption('de', 'German'),
        createOption('fr', 'French'),
        createOption('it', 'Italian')
      ], 'Language'),
      createBreak(),
      createInput('amount', 1000, 'Amount'),
      createBreak(),
      createInput('stored_customer_firstname', 'John', 'Firstname'),
      createInput('stored_customer_lastname', 'Doe', 'Lastname'),
      createBreak(),
      createDiv(targetSelector + '-vis', null, [
        createInput('cardno', '4242424242424242', 'Card number'),
        createInput('cvv', '123', 'CVV'),
        createBreak(),
        createInput('expm', '12', 'Month'),
        createInput('expy', '18', 'Year')
      ]),
      createDiv(targetSelector + '-dd', null, [
        createInput('stored_customer_street', 'Wallstreet', 'Street'),
        createInput('stored_customer_zip_code', '1337', 'Zip Code'),
        createInput('stored_customer_city', 'New York', 'City'),
        createBreak(),
        createInput('iban', 'CH9300762011623852957', 'IBAN'),
        createInput('bank_name', 'UBS', 'Bank Name'),
        createBreak(),
        createInput('bank_zip_code', '8005', 'Bank Zip Code'),
        createInput('bank_city', 'Zürich', 'Bank City'),
      ]),
      createDiv(targetSelector + '-ebs', null, []),
      createDiv(targetSelector + '-elv', null, [
        createInput('bankiban', 'CH9300762011623852957', 'IBAN'),
      ]),
      createDiv(targetSelector + '-eps', null, []),
      createDiv(targetSelector + '-mpw', null, [
        // todo: check (account)
      ]),
      createDiv(targetSelector + '-es', null, []),
      createDiv(targetSelector + '-ezs', null, [
        createInput('payer_line1', 'Payer line 1', 'Payer line 1'),
        createInput('payer_line2', 'Payer line 2', 'Payer line 2'),
        createBreak(),
        createInput('payer_line3', 'Payer line 3', 'Payer line 3'),
        createInput('payer_line4', 'Payer line 4', 'Payer line 4'),
        createBreak(),
        createInput('refno', 'Refno', 'Reference number'),
      ]),
      createDiv(targetSelector + '-pp', null, [
        // todo: check (account)
      ]),
      createDiv(targetSelector + '-pfc', null, []),
      createDiv(targetSelector + '-pef', null, [
        // todo: check (account)
      ]),
      createDiv(targetSelector + '-sod', null, [
        // todo: check iban
      ]),
      createBreak(),
      createSubmit('Submit'),
      createBreak(),
      createDiv(targetSelector + '-errors', 'payment-errors'),
      createBreak(),
      createDiv(targetSelector + '-credit-card-iframe', 'credit-card-iframe'),
      createBreak(),
      createDiv(targetSelector + '-payment-iframe', 'payment-iframe'),
      createBreak(),
      createDiv(targetSelector + '-response', 'payment-response')
    ])

    form.addEventListener('submit', (event: Event) => {
      event.preventDefault()
      event.stopPropagation()

      this.submitForm(this.getFormData())
    })

    each(form.querySelectorAll('select'), (el: HTMLSelectElement) => {
      el.addEventListener('change', () => this.dataChanged())
    })
    each(form.querySelectorAll('input'), (el: HTMLInputElement) => {
      el.addEventListener('keyup', () => this.dataChanged())
      el.addEventListener('change', () => this.dataChanged())
    })

    findElement(targetSelector).appendChild(form)

    return form
  }

  private async submitForm(data: any): Promise<any> {
    this.errors = null
    this.response = null

    this.dataChanged()

    this.payment.update(data)
    let errors = await this.payment.validate()

    if (errors) {
      this.handleValidationErrors(errors)
    } else {
      let response = await this.payment.send()
      this.handlePaymentResponse(response)
    }
  }

  private getFormData(): any {
    return {
      flow: this.form['flow'].value,
      payment_method: this.form['payment_method'].value,
      amount: this.form['amount'].value,
      currency: this.form['currency'].value,
      language: this.form['language'].value,
      test_mode: this.form['test_mode'].checked,
      cardno: this.form['cardno'].value,
      cvv: this.form['cvv'].value,
      expm: this.form['expm'].value,
      expy: this.form['expy'].value,
      stored_customer_firstname: this.form['stored_customer_firstname'].value,
      stored_customer_lastname: this.form['stored_customer_lastname'].value,
      stored_customer_street: this.form['stored_customer_street'].value,
      stored_customer_zip_code: this.form['stored_customer_zip_code'].value,
      stored_customer_city: this.form['stored_customer_city'].value,
      iban: this.form['iban'].value,
      bankiban: this.form['bankiban'].value,
      bank_name: this.form['bank_name'].value,
      bank_city: this.form['bank_city'].value,
      bank_zip_code: this.form['bank_zip_code'].value,
      payer_line1: this.form['payer_line1'].value,
      payer_line2: this.form['payer_line2'].value,
      payer_line3: this.form['payer_line3'].value,
      payer_line4: this.form['payer_line4'].value,
      refno: this.form['refno'].value,
    }
  }

  private dataChanged() {
    let data = this.getFormData()

    console.debug('form', data)

    let groups = ['vis', 'dd', 'ebs', 'elv', 'eps', 'mpw', 'es', 'ezs', 'pp', 'pfc', 'pef', 'sod']


    groups.forEach((groupId) => {
      let group = findElement(this.targetSelector + '-' + groupId)

      if (groupId === data.payment_method) {
        group.style.display = 'block'
      } else {
        group.style.display = 'none'
      }

      let creditCardIframeTarget = findElement(this.targetSelector + '-credit-card-iframe')

      if (groupId === 'vis') {
        if (data.payment_method === 'vis' && this.form['credit-card-iframe'].checked) {
          group.style.display = 'none'
          creditCardIframeTarget.style.display = 'block'
          this.payment.showCreditCardIframe()
        } else {
          creditCardIframeTarget.style.display = 'none'
          this.payment.hideCreditCardIframe()
        }
      }
    })

    ////////////////////////////////////////////////////////////////////////////////

    let errorsTarget = findElement(this.targetSelector + '-errors')

    if (this.errors) {
      errorsTarget.style.display = 'block'
      errorsTarget.innerHTML = ''

      each(this.errors, (value, key) => {
        let div = createDiv()
        div.innerHTML = key + ': ' + value

        errorsTarget.appendChild(div)
      })
    } else {
      errorsTarget.style.display = 'none'
    }

    ////////////////////////////////////////////////////////////////////////////////

    let responseTarget = findElement(this.targetSelector + '-response')

    if (this.response) {
      responseTarget.style.display = 'block'
      responseTarget.innerHTML = ''

      each(this.response, (value, key) => {
        let div = createDiv()
        div.innerHTML = key + ': ' + value

        responseTarget.appendChild(div)
      })
    } else {
      responseTarget.style.display = 'none'
    }
  }

  private handleValidationErrors(errors: PaymentErrors) {
    this.errors = errors
    this.dataChanged()
  }

  private handlePaymentResponse(response: PaymentResponse) {
    this.response = response
    this.dataChanged()
  }
}

export function createDemoWidget(targetSelector: string, epik: Epik) {
  let widget = new DemoWidget(targetSelector, epik)
}
