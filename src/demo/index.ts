import {createEpik} from '../epik/Epik'
import {createDemoWidget} from './DemoWidget'
import {Eventer} from '../shared/Eventer'

(async function () {
  let epik = createEpik({
    apiKey: '1234567890',
    // errorUrl: 'http://echo.maxim.io?error',
    // successUrl: 'http://echo.maxim.io?success',
    // cancelUrl: 'http://echo.maxim.io?cancel',
  })

  let widget1 = createDemoWidget('#widget1', epik.clone({
    creditCardIframeTargetSelector: '#widget1-credit-card-iframe',
    paymentIframeTargetSelector: '#widget1-payment-iframe',
  }))

  let widget2 = createDemoWidget('#widget2', epik.clone({
    creditCardIframeTargetSelector: '#widget2-credit-card-iframe',
    paymentIframeTargetSelector: '#widget2-payment-iframe',
  }))
})()
