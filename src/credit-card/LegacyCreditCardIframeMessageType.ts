export enum LegacyCreditCardIframeMessageType {
  DATA_CHANGED = 'rnw-creditcardform.data-changed',

  CREATE_TOKEN = 'rnw-creditcardform.create-token',
  TOKEN_RECEIVED = 'rnw-creditcardform.token-received',
  CREATE_TOKEN_ERROR = 'rnw-creditcardform.create-token-error',

  VALIDATE_FORM = 'rnw-creditcardform.validate-form',
  FORM_VALID = 'rnw-creditcardform.form-valid',
  FORM_NOT_VALID = 'rnw-creditcardform.form-not-valid',
}
