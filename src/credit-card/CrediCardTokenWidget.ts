import qs from 'qs'
import {CreditCardIframeMessageType} from '../shared/CreditCardIframeMessageType'
import {createAndInjectIframe} from '../shared/Dom'
import {isArray, keys} from '../shared/lodash'
import {Messenger} from '../shared/Messenger'
import {LegacyCreditCardIframeMessageType} from './LegacyCreditCardIframeMessageType'

export class CreditCardTokenWidget {
  private win: Window
  private parentMessenger: Messenger
  private iframeMessenger: Messenger
  private previousPaymentMethod: string = null

  constructor(
    private apiKey: string,
  ) {
    this.win = window

    this.setup()
  }

  private async setup() {
    this.parentMessenger = new Messenger(window, window.parent, 'slave')
    await this.parentMessenger.establishLinkBetweenSlaveAndMaster()

    let iframe = await createAndInjectIframe(this.win.document, {
      src: 'http://api.raisenow.com/epayment/api/step/select-card/merchant/1234567890?submit_button_visibility=false'
    })

    this.iframeMessenger = new Messenger(window, iframe.contentWindow)

    ////////////////////////////////////////////////////////////////////////////

    this.iframeMessenger.on(LegacyCreditCardIframeMessageType.DATA_CHANGED, (data) => {
      let previousPaymentMethod = this.previousPaymentMethod
      let paymentMethod = data.paymentMethod || null
      this.previousPaymentMethod = paymentMethod

      if (previousPaymentMethod !== paymentMethod) {
        this.parentMessenger.emit(CreditCardIframeMessageType.CREDIT_CARD_TYPE_CHANGED, {paymentMethod})
      }

      this.parentMessenger.emit(CreditCardIframeMessageType.DATA_CHANGED, {paymentMethod})
    })

    ////////////////////////////////////////////////////////////////////////////

    this.parentMessenger.respond(CreditCardIframeMessageType.DETECT_CREDIT_CARD_TYPE, (data) => {
      return this.previousPaymentMethod
    })

    ////////////////////////////////////////////////////////////////////////////

    let tokenizePromiseResolvers = []

    this.parentMessenger.respond(CreditCardIframeMessageType.TOKENIZE, (data) => {
      return new Promise((resolve, reject) => {
        tokenizePromiseResolvers.push(resolve)
        this.iframeMessenger.emit(LegacyCreditCardIframeMessageType.CREATE_TOKEN, {
          apiKey: this.apiKey
        })
      })
    })

    this.iframeMessenger.on(LegacyCreditCardIframeMessageType.TOKEN_RECEIVED, (data) => {
      let token = data.token

      tokenizePromiseResolvers.forEach((resolve) => resolve(token))
      tokenizePromiseResolvers = []

      this.parentMessenger.emit(CreditCardIframeMessageType.TOKENIZE_SUCCESS, token)
    })

    this.iframeMessenger.on(LegacyCreditCardIframeMessageType.CREATE_TOKEN_ERROR, (data) => {
      tokenizePromiseResolvers.forEach((resolve) => resolve(null))
      tokenizePromiseResolvers = []

      this.parentMessenger.emit(CreditCardIframeMessageType.TOKENIZE_ERROR)
    })

    this.iframeMessenger.on(LegacyCreditCardIframeMessageType.FORM_NOT_VALID, (data) => {
      // calling tokenize in iframe, also runs validators, if there
      // are any validation errors, iframe won't emit tokenize-error,
      // instead it's gonna emit validate-error, so we have to resolve
      // our tokenize promises with null
      this.iframeMessenger.simulate(LegacyCreditCardIframeMessageType.CREATE_TOKEN_ERROR)
    })

    ////////////////////////////////////////////////////////////////////////////

    let validatePromiseResolvers = []

    this.parentMessenger.respond(CreditCardIframeMessageType.VALIDATE, (data) => {
      return new Promise((resolve, reject) => {
        validatePromiseResolvers.push(resolve)
        this.iframeMessenger.emit(LegacyCreditCardIframeMessageType.VALIDATE_FORM)
      })
    })

    this.iframeMessenger.on(LegacyCreditCardIframeMessageType.FORM_NOT_VALID, (data) => {
      let errors = {}

      // transform list of invalid keys, into standard error messages
      if (isArray(data)) {
        data.forEach((v) => errors[v] = 'Invalid value')
      }

      errors = keys(errors).length > 0 ? errors : null

      validatePromiseResolvers.forEach((resolve) => resolve(errors))
      validatePromiseResolvers = []
    })

    this.iframeMessenger.on(LegacyCreditCardIframeMessageType.FORM_VALID, (data) => {
      validatePromiseResolvers.forEach((resolve) => resolve(null))
      validatePromiseResolvers = []

      this.parentMessenger.emit(CreditCardIframeMessageType.VALIDATE_SUCCESS)
    })
  }
}

export function createCreditCardTokenWidget () {
  let query = qs.parse(window.location.search, {ignoreQueryPrefix: true})
  let apiKey = query.apiKey

  return new CreditCardTokenWidget(apiKey)
}
