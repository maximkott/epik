import {includes} from '../shared/lodash'
import {CreditCardType, getCreditCardType} from "./CreditCardType";
import {
  validateLengthBetween, validateNumberBetween,
  validateNumerical
} from './genericValidation'

export function validateCreditCardNumber(cardNumber: string): boolean {
  return validateNumerical(cardNumber) && validateCreditCardLength(cardNumber) && validateCreditCardLuhn(cardNumber)
}

function validateCreditCardLength(cardNumber: string): boolean {
  let type = getCreditCardType(cardNumber)

  if (type === CreditCardType.AMERICAN_EXPRESS) {
    return includes([15], cardNumber.length)
  } else if (type === CreditCardType.VISA ) {
    return includes([13, 16, 19], cardNumber.length)
  } else if (type === CreditCardType.MASTER_CARD ) {
    return includes([16], cardNumber.length)
  } else if (type === CreditCardType.DISCOVER ) {
    return includes([16, 17, 18, 19], cardNumber.length)
  } else if (type === CreditCardType.DINERS_CLUB) {
    return includes([14, 15, 16, 17, 18, 19], cardNumber.length)
  } else if (type === CreditCardType.JCB) {
    return includes([16, 17, 18, 19], cardNumber.length)
  } else if (type === CreditCardType.MAESTRO) {
    return includes([12, 13, 14, 15, 16, 17, 18, 19], cardNumber.length)
  } else {
    return false
  }
}

function validateCreditCardLuhn(cardNumber: string): boolean {
  let s = 0
  let doubleDigit = false

  for (let i = cardNumber.length - 1; i >= 0; i--) {
    let digit = +cardNumber[i]
    if (doubleDigit) {
      digit *= 2
      if (digit > 9) {
        digit -= 9
      }
    }
    s += digit
    doubleDigit = !doubleDigit
  }

  return s % 10 === 0
}

export function validateCreditCardCvv(input: string): boolean {
  return validateNumerical(input) && validateLengthBetween(input, 3, 4)
}

export function validateCreditCardExpirationMonth(input: string): boolean {
  return validateNumberBetween(input, 1, 12)
}

export function validateCreditCardExpirationYear(input: string): boolean {
  let currentYear = (new Date).getFullYear() - 2000

  return validateNumberBetween(input, currentYear, currentYear + 50)
}
