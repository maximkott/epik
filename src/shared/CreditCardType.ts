import {validateNumberBetween, validateNumberOneOf} from './genericValidation'

export enum CreditCardType {
  AMERICAN_EXPRESS = 'amx',
  VISA = 'vis',
  MASTER_CARD = 'eca',
  DISCOVER = 'dis',
  DINERS_CLUB = 'din',
  JCB = 'jcb',
  MAESTRO = 'mae',
}

export function getCreditCardType (cardNumber: string, allowedTypes?: string[]): CreditCardType {
  if ( ! cardNumber) {
    return null
  }

  let type = null

  let firstDigit = parseInt(cardNumber.substr(0, 1))
  let firstTwoDigits = parseInt(cardNumber.substr(0, 2))
  let firstThreeDigits = parseInt(cardNumber.substr(0, 3))
  let firstFourDigits = parseInt(cardNumber.substr(0, 4))

  if (validateNumberOneOf(firstTwoDigits, [34, 37])) {
    type = CreditCardType.AMERICAN_EXPRESS
  } else if (firstDigit === 4) {
    type = CreditCardType.VISA
  } if (validateNumberBetween(firstTwoDigits, 51, 55) || validateNumberBetween(firstFourDigits, 2221, 2720)) {
    type = CreditCardType.MASTER_CARD
  } if (firstFourDigits === 6011 || validateNumberOneOf(firstTwoDigits, [64, 65])) {
    type = CreditCardType.DISCOVER
  } if (firstFourDigits === 3095 || validateNumberOneOf(firstTwoDigits, [36, 38, 39, 54, 55]) || validateNumberBetween(firstThreeDigits, 300, 305)) {
    type = CreditCardType.DINERS_CLUB
  } if (validateNumberBetween(firstFourDigits, 3528, 3589)) {
    type = CreditCardType.JCB
  } else if (firstDigit === 6 || firstTwoDigits === 50 || validateNumberBetween(firstTwoDigits, 56, 58)) {
    type = CreditCardType.MAESTRO
  }

  if (allowedTypes && allowedTypes.indexOf(type) === -1) {
    return null
  }

  return type
}
