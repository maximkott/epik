export const CreditCardIframeMessageType = {
  VALIDATE: 'rnw.credit-card.validate',
  VALIDATE_SUCCESS: 'rnw.credit-card.validate:success',
  VALIDATE_ERROR: 'rnw.credit-card.validate:error',

  TOKENIZE: 'rnw.credit-card.tokenize',
  TOKENIZE_SUCCESS: 'rnw.credit-card.tokenize:success',
  TOKENIZE_ERROR: 'rnw.credit-card.tokenize:error',

  DETECT_CREDIT_CARD_TYPE: 'rnw.credit-card.detect-credit-card-type',

  DATA_CHANGED: 'rnw.credit-card.data:changed',
  CREDIT_CARD_TYPE_CHANGED: 'rnw.credit-card.credit-card-type:changed',
}
