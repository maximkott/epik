// https://en.wikipedia.org/wiki/International_Bank_Account_Number

const IBAN_FORMATS = {
  // for items below:
  //    kk = represent the IBAN checksum
  //    b = National bank code
  //    s = Branch code
  //    x = National check digit
  //    c = Account number
  //    a - Balance account number
  //    t = Account type
  //    n = Owner account number
  //    m = Currency
  //    i = holder's identification number
  //    0 = Zeroes
  'Albania': 'ALkk bbbs sssx cccc cccc cccc cccc',
  'Andorra': 'ADkk bbbb ssss cccc cccc cccc',
  'Austria': 'ATkk bbbb bccc cccc cccc',
  'Azerbaijan': 'AZkk bbbb cccc cccc cccc cccc cccc',
  'Bahrain': 'BHkk bbbb cccc cccc cccc cc',
  'Belarus': 'BYkk bbbb aaaa cccc cccc cccc cccc',
  'Belgium': 'BEkk bbbc cccc ccxx',
  'Bosnia and Herzegovina': 'BAkk bbbs sscc cccc ccxx',
  'Brazil': 'BRkk bbbb bbbb ssss sccc cccc ccct n',
  'Bulgaria': 'BGkk bbbb ssss ttcc cccc cc',
  'Costa Rica': 'CRkk 0bbb cccc cccc cccc cc',
  'Croatia': 'HRkk bbbb bbbc cccc cccc c',
  'Cyprus': 'CYkk bbbs ssss cccc cccc cccc cccc',
  'Czech Republic': 'CZkk bbbb ssss sscc cccc cccc',
  'Denmark': 'DKkk bbbb cccc cccc cc',
  'Dominican Republic': 'DOkk bbbb cccc cccc cccc cccc cccc',
  'East Timor': 'TLkk bbbc cccc cccc cccc cxx',
  'Estonia': 'EEkk bbss cccc cccc cccx',
  'Faroe Islands': 'FOkk bbbb cccc cccc cx',
  'Finland': 'FIkk bbbb bbcc cccc cx',
  'France': 'FRkk bbbb bsss sscc cccc cccc cxx',
  'Georgia': 'GEkk bbcc cccc cccc cccc cc',
  'Germany': 'DEkk bbbb bbbb cccc cccc cc',
  'Gibraltar': 'GIkk bbbb cccc cccc cccc ccc',
  'Greece': 'GRkk bbbs sssc cccc cccc cccc ccc',
  'Greenland': 'GLkk bbbb cccc cccc cc',
  'Guatemala': 'GTkk bbbb mmtt cccc cccc cccc cccc',
  'Hungary': 'HUkk bbbs sssx cccc cccc cccc cccx',
  'Iceland': 'ISkk bbbb sscc cccc iiii iiii ii',
  'Ireland': 'IEkk aaaa bbbb bbcc cccc cc',
  'Israel': 'ILkk bbbn nncc cccc cccc ccc',
  'Italy': 'ITkk xbbb bbss sssc cccc cccc ccc',
  'Jordan': 'JOkk bbbb ssss cccc cccc cccc cccc cc',
  'Kazakhstan': 'KZkk bbbc cccc cccc cccc',
  'Kosovo': 'XKkk bbbb cccc cccc cccc',
  'Kuwait': 'KWkk bbbb cccc cccc cccc cccc cccc cc',
  'Latvia': 'LVkk bbbb cccc cccc cccc c',
  'Lebanon': 'LBkk bbbb cccc cccc cccc cccc cccc',
  'Liechtenstein': 'LIkk bbbb bccc cccc cccc c',
  'Lithuania': 'LTkk bbbb bccc cccc cccc',
  'Luxembourg': 'LUkk bbbc cccc cccc cccc',
  'Macedonia': 'MKkk bbbc cccc cccc cxx',
  'Malta': 'MTkk bbbb ssss sccc cccc cccc cccc ccc',
  'Mauritania': 'MRkk bbbb bsss sscc cccc cccc cxx',
  'Mauritius': 'MUkk bbbb bbss cccc cccc cccc 000m mm',
  'Monaco': 'MCkk bbbb bsss sscc cccc cccc cxx',
  'Moldova': 'MDkk bbcc cccc cccc cccc cccc',
  'Montenegro': 'MEkk bbbc cccc cccc cccc xx',
  'Netherlands': 'NLkk bbbb cccc cccc cc',
  'Norway': 'NOkk bbbb cccc ccx',
  'Pakistan': 'PKkk bbbb cccc cccc cccc cccc',
  'Palestinian territories': 'PSkk bbbb xxxx xxxx xccc cccc cccc c',
  'Poland': 'PLkk bbbs sssx cccc cccc cccc cccc',
  'Portugal': 'PTkk bbbb ssss cccc cccc cccx x',
  'Qatar': 'QAkk bbbb cccc cccc cccc cccc cccc c',
  'Romania': 'ROkk bbbb cccc cccc cccc cccc',
  'San Marino': 'SMkk xbbb bbss sssc cccc cccc ccc',
  'Saudi Arabia': 'SAkk bbcc cccc cccc cccc cccc',
  'Serbia': 'RSkk bbbc cccc cccc cccc xx',
  'Slovakia': 'SKkk bbbb ssss sscc cccc cccc',
  'Slovenia': 'SIkk bbss sccc cccc cxx',
  'Spain': 'ESkk bbbb ssss xxcc cccc cccc',
  'Sweden': 'SEkk bbbc cccc cccc cccc cccc',
  'Switzerland': 'CHkk bbbb bccc cccc cccc c',
  'Tunisia': 'TNkk bbss sccc cccc cccc cccc',
  'Turkey': 'TRkk bbbb bxcc cccc cccc cccc cc',
  'United Arab Emirates': 'AEkk bbbc cccc cccc cccc ccc',
  'United Kingdom': 'GBkk bbbb ssss sscc cccc cc',
  'Virgin Islands, British': 'VGkk bbbb cccc cccc cccc cccc',

  // for items below:
  //    kk = represent the IBAN checksum
  //    a = represents an alphabetic character
  //    n = represents a numeric character
  //    b = represents a bank code character
  //    c = represents an account digit.
  //    0 = represents a "0" character
  'Algeria': 'DZkk nnnn nnnn nnnn nnnn nnnn',
  'Angola': 'AOkk nnnn nnnn nnnn nnnn nnnn n',
  'Benin': 'BJkk annn nnnn nnnn nnnn nnnn nnnn',
  'Burkina Faso': 'BFkk nnnn nnnn nnnn nnnn nnnn nnnn',
  'Burundi': 'BIkk nnnn nnnn nnnn',
  'Cameroon': 'CMkk nnnn nnnn nnnn nnnn nnnn nnn',
  'Cape Verde': 'CVkk nnnn nnnn nnnn nnnn nnnn n',
  'Iran': 'IRkk 0bb0 nnnn nnnn nnnn nnnn nn',
  'Ivory Coast': 'CIkk annn nnnn nnnn nnnn nnnn nnnn',
  'Madagascar': 'MGkk nnnn nnnn nnnn nnnn nnnn nnn',
  'Mali': 'MLkk annn nnnn nnnn nnnn nnnn nnnn',
  'Mozambique': 'MZkk nnnn nnnn nnnn nnnn nnnn n',
  'Senegal': 'SNkk annn nnnn nnnn nnnn nnnn nnnn',
  'Ukraine': 'UAkk bbbb bbcc cccc cccc cccc cccc c',
  'Comoros': 'KMkk nnnn nnnn nnnn nnnn nnnn nnn',
  'Chad': 'TDkk nnnn nnnn nnnn nnnn nnnn nnn',
  'Congo': 'CGkk nnnn nnnn nnnn nnnn nnnn nnn',
  'Egypt': 'EGkk nnnn nnnn nnnn nnnn nnnn nnn',
  'Gabon': 'GAkk nnnn nnnn nnnn nnnn nnnn nnn',
  'Honduras': 'HNkk aaaa nnnn nnnn nnnn nnnn nnnn',
  'Morocco': 'MAkk nnnn nnnn nnnn nnnn nnnn nnnn',
  'Nicaragua': 'NIkk aaaa nnnn nnnn nnnn nnnn nnnn nnnn',
  'Niger': 'NEkk aann nnnn nnnn nnnn nnnn nnnn',
  'Togo': 'TGkk aann nnnn nnnn nnnn nnnn nnnn'
}

function sanitizeIBAN (iban: string) {
  return (iban || '').toUpperCase().replace(/[^A-Z0-9]/g, '')
}

function getCountryCodeFromIBAN (iban: string) {
  return iban.substr(0, 2)
}

function getChecksumFromIBAN (iban: string) {
  return iban.substr(2, 2)
}

function getNumberFromIBAN (iban: string) {
  return iban.substr(4)
}

function findFormatForIBAN (iban: string) {
  let countryCode = getCountryCodeFromIBAN(iban)

  for (let countryName in IBAN_FORMATS) {
    let format = IBAN_FORMATS[countryName]

    if (getCountryCodeFromIBAN(format) === countryCode) {
      return IBAN_FORMATS[countryName]
    }
  }

  return null
}

function checkIBANLength (iban: string) {
  let ibanFormat = sanitizeIBAN(findFormatForIBAN(iban))

  return iban.length === ibanFormat.length
}

function checkIBANChecksum (iban: string) {
  let ibanCountryCode = getCountryCodeFromIBAN(iban)
  let ibanChecksum = getChecksumFromIBAN(iban)
  let ibanNumber = getNumberFromIBAN(iban)

  let reversedIban = ibanNumber + ibanCountryCode + ibanChecksum
  let ibanInteger = reversedIban.replace(/[A-Z]/g, (char) => (char.charCodeAt(0) - 55).toString())
  let ibanWithoutChecksum = ibanInteger.substr(2)

  let checksum = ibanInteger.substr(0, 2)

  for (let i = 0; i < ibanWithoutChecksum.length; i += 7) {0
    let fragment = checksum + ibanWithoutChecksum.substring(i, i + 7)
    checksum = (parseInt(fragment, 10) % 97).toString()
  }

  return checksum === '1'
}

export function validateIBAN (input: string) {
  let iban = sanitizeIBAN(input)

  return checkIBANLength(iban) && checkIBANChecksum(iban)
}

export function formatIBAN (input: string) {
  let iban = sanitizeIBAN(input)
  let ibanFormat = findFormatForIBAN(iban)

  ibanFormat.split('').forEach((char, index) => {
    if (char === ' ') {
      iban = iban.substr(0, index) + ' ' + iban.substr(index + 1)
    }
  })

  return iban
}
