import {isFunction, isObject, cloneDeep} from '../shared/lodash'

export type EventerEventListener = (data: any, topic?: string) => void
export type EventerEventResponder = (data: any, topic?: string) => any|Promise<any>
export type EventerEvent = {channel: string, topic: string, data: any}

const MASTER_SLAVE_HANDSHAKE = 'master-slave-handshake'

export interface IEventer {
  emit(topic: string, data?: any)
  on(topic: string, listener: EventerEventListener)
  simulate(topic: string, data?: any)
  request(topic: string, data?: any, listener?: EventerEventListener): Promise<any>
  respond(topic: string, responder: EventerEventResponder)
  once(topic: string, listener?: EventerEventListener): Promise<any>
  off(topic: string, listener: EventerEventListener)
}

export class Eventer implements IEventer {
  listeners: {[key: string]: EventerEventListener[]} = {}
  responders: {[key: string]: EventerEventResponder[]} = {}

  emit(topic: string, data?: any) {
    this.emitMessage(topic, this.createPayload(data))
  }

  on(topic: string, listener: EventerEventListener) {
    if (!isFunction(listener)) {
      throw new Error('Event listener must be a function')
    }

    if (!this.listeners[topic]) {
      this.listeners[topic] = []
    }

    this.listeners[topic].push(listener)
  }

  simulate(topic: string, data?: any) {
    this.receiveMessage(topic, data)
  }

  async request(topic: string, data?: any, listener?: EventerEventListener): Promise<any> {
    if (listener && !isFunction(listener)) {
      throw new Error('Event listener must be a function')
    }

    let respondTo = topic + '-response-' + Date.now()

    let promise = this.once(respondTo, listener)
    this.emitMessage(topic, this.createPayload(data, respondTo))

    return promise
  }

  respond(topic: string, responder: EventerEventResponder) {
    if (!isFunction(responder)) {
      throw new Error('Event responder must be a function')
    }

    if (!this.responders[topic]) {
      this.responders[topic] = []
    }

    this.responders[topic].push(responder)
  }

  async once(topic: string, listener?: EventerEventListener): Promise<any> {
    return new Promise((resolve, reject) => {
      let onceListener = (data: any) => {
        this.off(topic, onceListener)

        if (listener) {
          listener(data, topic)
        }

        resolve(data)
      }

      this.on(topic, onceListener)
    })
  }

  off(topic: string, listener: EventerEventListener) {
    let listeners = this.listeners[topic] || []
    let responders = this.responders[topic] || []

    for (let key in listeners) {
      if (listeners[key] === listener) {
        this.listeners[topic].splice(parseInt(key), 1)
        return
      }
    }

    for (let key in responders) {
      if (responders[key] === listener) {
        this.responders[topic].splice(parseInt(key), 1)
        return
      }
    }
  }

  protected emitMessage(topic: string, payload: any) {
    console.debug('eventer emit', {topic, data: {...payload}})

    this.notifyListeners(topic, cloneDeep(payload))
    this.notifyResponders(topic, cloneDeep(payload))
  }

  protected receiveMessage(topic: string, payload: any) {
    console.debug('eventer receive', {topic, data: {...payload}})

    this.notifyListeners(topic, cloneDeep(payload))
    this.notifyResponders(topic, cloneDeep(payload))
  }

  protected notifyListeners(topic: string, payload: any) {
    let {data} = this.parsePayload(payload)

    if (topic !== '*') {
      this.notifyListeners('*', payload)
    }

    let listeners = this.listeners[topic]

    if (listeners) {
      for (let listener of listeners) {
        listener(data, topic)
      }
    }
  }

  protected notifyResponders(topic: string, payload: any) {
    let {data, respondTo} = this.parsePayload(payload)

    let responders = this.responders[topic]

    if (responders) {
      for (let responder of responders) {
        let response

        try {
          response = responder(data, topic)
        } catch (err) {
          console.error(err)
        }

        if (respondTo) {
          if (response && response.then) {
            response.then((resolvedResponse: any) => {
              let {data} = this.parsePayload(resolvedResponse)

              this.emit(respondTo, data)
            }).catch((err: any) => {
              console.error(err)
            })
          } else {
            let {data} = this.parsePayload(response)

            this.emit(respondTo, data)
          }
        }
      }
    }
  }

  private createPayload(data: any, respondTo?: string) {
    if ( ! isObject(data)) {
      data = {__rawData: data}
    }

    if (respondTo) {
      data.__respondTo = respondTo
    }

    return data
  }

  private parsePayload(payload: any): {data: any, respondTo?: string} {
    payload = payload || {__rawData: null}

    let respondTo = payload.__respondTo || null
    delete payload.__respondTo

    let data = payload.__rawData || payload.__rawData === null ? payload.__rawData : payload

    return {data, respondTo}
  }
}
