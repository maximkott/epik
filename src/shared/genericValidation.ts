import {startsWith} from './lodash'

export function validateLength (input: string, length: number): boolean {
  return input.length === length
}

export function validateMinLength (input: string, length: number): boolean {
  return input.length >= length
}

export function validateMaxLength (input: string, length: number): boolean {
  return input.length <= length
}

export function validateLengthBetween (input: string, minLength: number, maxLength: number): boolean {
  return validateMinLength(input, minLength) && validateMaxLength(input, maxLength)
}

export function validateNumerical (input: string): boolean {
  return /^[.\d]+$/.test(input)
}

export function validateAlpha (input: string): boolean {
  return /^[A-z]+$/.test(input)
}

export function vlaidateAlphaNumerical (input: string): boolean {
  return /^[A-z0-9.]+$/.test(input)
}

export function validateNumberBetween (input: number|string, min: number, max: number): boolean {
  let number = parseFloat(input as string)

  return  number >= min && number <= max
}

export function validateNumberOneOf (input: number, numbers: number[]): boolean {
  for (let number of numbers) {
    if (input === number) {
      return true
    }
  }

  return false
}

export function validateInRange (input: any, range: any[]): boolean {
  return range.indexOf(input) !== -1
}

export function validateStartsWithOneOf(input: string, items: string[]): boolean {
  for (let item of items) {
    if (startsWith(input, item)) {
      return true
    }
  }

  return false
}
