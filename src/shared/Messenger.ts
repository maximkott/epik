import {cloneDeep} from '../shared/lodash'
import {Eventer, EventerEventListener, EventerEventResponder} from './Eventer'

export type MessengerType = 'master' | 'slave'
export type Message = {channel: string, topic: string, data: any}

const MASTER_SLAVE_HANDSHAKE = 'master-slave-handshake'

export class Messenger extends Eventer {
  private channel = null
  private linkEstablished = false
  private postMessageListenerAttached = false

  constructor(
    private listenTo?: Window,
    private emitTo?: Window,
    private type: MessengerType = null
  ) {
    super()

    if (listenTo && listenTo === emitTo) {
      throw new Error('Messenger received two windows of the same instance, this is probably an error, since you want one window to be your main window and the other to be an iframe')
    }

    if (type && type !== 'master' && type !== 'slave') {
      throw new Error('Messenger type must be either "master" or "slave"')
    }

    this.setupPostMessageListener()
  }

  emit(topic: string, data?: any) {
    this.checkLink(topic)

    if ( ! this.emitTo) return

    super.emit(topic, data)
  }

  on(topic: string, listener: EventerEventListener) {
    this.checkLink(topic)

    if ( ! this.listenTo) return

    super.on(topic, listener)
  }

  async request(topic: string, data?: any, listener?: EventerEventListener): Promise<any> {
    this.checkLink(topic)

    if ( ! this.emitTo || ! this.listenTo) return

    return super.request(topic, data, listener)
  }

  respond(topic: string, responder: EventerEventResponder) {
    this.checkLink(topic)

    if ( ! this.emitTo || ! this.listenTo) return

    super.respond(topic, responder)
  }

  async once(topic: string, listener?: EventerEventListener): Promise<any> {
    this.checkLink(topic)

    if (! this.listenTo) return

    return super.once(topic, listener)
  }

  async establishLinkBetweenSlaveAndMaster() {
    if (this.linkEstablished) {
      throw new Error('Link has already been established')
    }

    if (this.type === 'master') {
      await this.setupMaster()
    } else if (this.type === 'slave') {
      await this.setupSlave()
    }

    this.linkEstablished = true
  }

  private async setupMaster() {
    this.channel = this.generateChannelId()

    await this.request(MASTER_SLAVE_HANDSHAKE, {channel: this.channel})
  }

  private async setupSlave() {
    await this.respond(MASTER_SLAVE_HANDSHAKE, (data) => {
      this.channel = data.channel
    })
  }

  private setupPostMessageListener() {
    if (this.listenTo) {
      if (this.postMessageListenerAttached) {
        throw new Error('Post message listener has already been attached')
      }

      this.listenTo.addEventListener('message', (event: MessageEvent) => {
        try {
          let data = JSON.parse(event.data || '') || {}
          let message = this.createPostMessage(data.channel, data.topic, data.data)

          this.receivePostMessage(message)
        } catch (err) {
          // throw new Error('Could not handle message, got error: ' + err.toString())
        }
      })

      this.postMessageListenerAttached = true
    }
  }

  protected emitMessage(topic: string, payload: any) {
    try {
      let message = this.createPostMessage(this.channel, topic, payload)
      this.emitPostMessage(message)
    } catch (err) {
      throw new Error('Could not emit message, got error: ' + err.toString())
    }
  }

  protected receiveMessage(topic: string, payload: any) {
    this.notifyListeners(topic, cloneDeep(payload))
    this.notifyResponders(topic, cloneDeep(payload))
  }

  private receivePostMessage(message: Message) {
    if (message && message.topic == null) {
      return
    }

    if (message.channel !== this.channel && ! this.isPrivateTopic(message.topic)) {
      return
    }

    console.debug('messenger ' + (this.type || '') + ' receive', cloneDeep(message))

    this.receiveMessage(message.topic, message.data)
  }

  private emitPostMessage(message: Message) {
    if ( ! this.emitTo) {
      throw new Error('Can not emit, emit target has not been set')
    }

    try {
      console.debug('messenger ' + (this.type || '') + ' emit', cloneDeep(message))

      this.emitTo.postMessage(JSON.stringify(message), '*')
    } catch (err) {
      throw new Error('Could not send message, got error: ' + err.toString())
    }
  }

  private createPostMessage(channel: string, topic: string, payload: any): Message {
    channel = channel || null
    topic = topic || null

    let message = {channel, topic, data: payload}

    return message
  }

  private checkLink(topic: string) {
    if (this.type && !this.linkEstablished && ! this.isPrivateTopic(topic)) {
      throw new Error('Link between windows has not been established yet, call the "await messenger.establishLinkBetweenSlaveAndMaster()" or "messenger.establishLinkBetweenSlaveAndMaster().then(...)" method to do so')
    }
  }

  private generateChannelId (): string {
    return 'rnw.epik.' + Date.now().toString()
  }

  private isPrivateTopic(topic: string): boolean {
    return topic.indexOf(MASTER_SLAVE_HANDSHAKE) === 0 && !! this.type
  }
}
