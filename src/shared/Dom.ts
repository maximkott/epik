import {isFunction} from '../shared/lodash'

export type IframeOptions = {
  name?: string
  src?: string
  style?: string
  onload?: (event: HTMLIFrameElement) => void
  hidden?: boolean
}

export async function createAndInjectIframe(document: Document, options?: IframeOptions, targetSelector?: string): Promise<HTMLIFrameElement> {
  return new Promise<HTMLIFrameElement>((resolve, reject) => {
    options = options || {}

    let iframe = document.createElement('iframe') as HTMLIFrameElement

    if (options.name) {
      iframe.setAttribute('name', options.name)
    }

    if (options.src) {
      iframe.setAttribute('src', options.src)
    }

    if (options.style) {
      iframe.setAttribute('style', options.style)
    }

    if (options.hidden === true) {
      iframe.setAttribute('style', 'display: none;')
    }

    iframe.onload = (event: Event) => {
      if (options && isFunction(options.onload)) {
        options.onload(iframe)
      }

      resolve(iframe)
    }

    injectHtmlElement(document, iframe, targetSelector)
  })
}

export type FormOptions = {
  target?: '_self' | string
  action?: string
  method?: 'get' | 'post'
  hidden?: boolean
}

export function createForm(document: Document, options: FormOptions): HTMLFormElement {
  let form = document.createElement('form')

  options = options || {}
  options.target = options.target || '_self'
  options.method = options.method || 'post'

  form.target = options.target
  form.method = options.method

  if (options.action) {
    form.action = options.action
  }

  if (options.hidden === true) {
    form.setAttribute('style', 'display: none;')
  }

  return form
}

export function createAndInjectForm(document: Document, options: FormOptions, targetSelector?: string): HTMLFormElement {
  let form = createForm(document, options)
  injectHtmlElement(document, form, targetSelector)

  return form
}

export function injectHtmlElement(document: Document, element: HTMLElement, targetSelector?: string) {
  if (targetSelector) {
    let target = findElement(targetSelector)

    if (!target) {
      throw new Error('Could not inject element, target selector "' + targetSelector + '" is invalid')
    }

    // clear previous contents
    target.innerHTML = ''
    target.appendChild(element)
  } else {
    document.body.appendChild(element)
  }
}

export function fillForm(document: Document, form: HTMLElement, data: { [key: string]: scalar }): void {
  for (let key in data) {
    let value = data[key]

    let input = document.createElement('input')
    input.setAttribute('type', 'text')
    input.setAttribute('name', key)
    input.setAttribute('placeholder', key)
    input.setAttribute('value', value as string)
    form.appendChild(input)
  }
}

export type PopupOptions = {
  location?: string
  name?: string
  features?: string
  onload?: (popup: Window) => void
}

export function createAndOpenPopup(window: Window, options?: PopupOptions): Window {
  options = options || {} as PopupOptions
  options.location = options.location || ''
  options.name = options.name || ''
  options.features = options.features || ''

  let popup = window.open(options.location, options.name, options.features)

  return popup
}

export function findElement (selector: string): HTMLElement {
  return document.querySelector(selector)
}
